extends Node

# Config variables

var Config := ConfigFile.new()

# Connection variables

var NetworkENet : NetworkedMultiplayerENet
var ValidConnection : bool = false

# Inbuilt functions

func _ready() -> void:
	if Config.load(get_config_path()) != OK:
		get_tree().change_scene("res://Scenes/Menus/SettingsMenu.tscn")
	else:
		Config.load(get_config_path())
		connect_to_server()

# Config functions

# Returns the location of the config file
func get_config_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Client.cfg")

# Connection functions

func connect_to_server() -> void:
	print("Attempting to connect to server")
	Settings.Username = Config.get_value("User", "Username")
	Settings.HostIp = Config.get_value("Connection", "HostIp")
	Settings.HostPort = Config.get_value("Connection", "HostPort")
	NetworkENet = NetworkedMultiplayerENet.new()
	NetworkENet.create_client(Settings.HostIp, Settings.HostPort)
	get_tree().set_network_peer(NetworkENet)
	NetworkENet.connect("connection_succeeded", self, "connection_succeeded")
	NetworkENet.connect("connection_failed", self, "connection_failed")

func connection_succeeded() -> void:
	print("Connection successful")
	ValidConnection = true
	if get_tree().current_scene.name == "SettingsMenu":
		get_node("/root/SettingsMenu").enable_main_menu()

func connection_failed() -> void:
	print("Connection failed")

func close_server_connection() -> void:
	print("Closing connection to server")
	NetworkENet.close_connection()

remote func username_approved() -> void:
	print("Username approved")

remote func username_rejected() -> void:
	print("Username rejected")

# Main menu functions

remote func set_game_setup_options(maxplayers : int, ticklengths : Array) -> void:
	get_node("/root/MainMenu").set_game_setup_options(maxplayers, ticklengths)

remote func set_joined_games(games : Dictionary) -> void:
	get_node("/root/MainMenu").set_joined_games(games)

remote func set_open_games(games : Dictionary) -> void:
	get_node("/root/MainMenu").set_open_games(games)

remote func add_joined_game(gameid : int, gameinfo : Dictionary) -> void:
	print("Joining game")
	get_node("/root/MainMenu").remove_open_game(gameid)
	get_node("/root/MainMenu").add_joined_game(gameid, gameinfo)

# Game functions

remote func set_game_variable(variable : String, value) -> void:
	if GameVariables.get(variable) != null:
		GameVariables.set(variable, value)
		match variable:
			"StartTime":
				if value != -1:
					if get_tree().current_scene.name == "Game":
						get_node("/root/Game").unpause_launch_timers()
	else:
		# Failure case
		print("\"" + variable + "\" is not a valid game variable")

remote func calculate_game_state(orders : Array) -> void:
	GameVariables.Orders = orders
	GameState.calculate_game_state(orders)

remote func set_global_tick(globaltick : int) -> void:
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").set_global_tick(globaltick)

remote func set_player(player : String, value : Dictionary) -> void:
	GameVariables.Players[player] = value
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").update_players()

remote func remove_online_player(player : String) -> void:
	GameVariables.OnlinePlayers.erase(player)
	get_node("/root/Game").update_online_players()

remote func add_online_player(player : String) -> void:
	if GameVariables.OnlinePlayers.has(player) == false:
		GameVariables.OnlinePlayers.append(player)
	get_node("/root/Game").update_online_players()

remote func add_message(chat : Array, message : Dictionary) -> void:
	if GameVariables.Chats.has(chat) == false:
		GameVariables.Chats[chat] = []
	GameVariables.Chats[chat].append(message)
	get_node("/root/Game").update_messages(chat)

remote func calculate_order(order : Dictionary) -> void:
	GameVariables.Orders.append(order)
	OrderFunctions.calculate_order(order)

remote func prepare_submarine_troops(submarineid : int, troops : int) -> void:
	var SubmarineName : String = DataFunctions.get_submarine_name_from_id(submarineid)
	get_node("/root/Game").override_temporary_troops(SubmarineName)
	GameState.prepare_submarine_troops(SubmarineName, troops)

remote func cancel_submarine(submarineid : int) -> void:
	print("Cancelling submarine")
	var SubmarineName : String = DataFunctions.get_submarine_name_from_id(submarineid)
	get_node("/root/Game").override_temporary_troops(SubmarineName)
	get_node("/root/Game").cancel_submarine(SubmarineName)
	GameState.cancel_submarine(SubmarineName)

remote func cancel_gift_submarine(submarineid : int) -> void:
	print("Cancelling submarine gift")
	var SubmarineName : String = DataFunctions.get_submarine_name_from_id(submarineid)
	GameState.cancel_gift_submarine(SubmarineName)

remote func focus_next_submarine() -> void:
	GameState.FocusNextSubmarine = true
