extends Node2D

onready var Generator : PackedScene = preload("res://Scenes/Outposts/Generator.tscn")
onready var Factory : PackedScene = preload("res://Scenes/Outposts/Factory.tscn")
onready var Submarine : PackedScene = preload("res://Scenes/Submarines/Submarine.tscn")
onready var SubmarineLines : PackedScene = preload("res://Scenes/Lines/SubmarineLines.tscn")
onready var OnlineIndicator : PackedScene = preload("res://Scenes/InterfaceElements/OnlineIndicator.tscn")

export var DistanceToHoverOutpost : float = 50.0
export var MapCursorMovementAllowance : float = 10.0

var Jumping : bool = false
var CursorPosition : Vector2
var OldMapCursorPosition : Vector2
var OldCameraPosition : Vector2
var HoveredStartOutpost : Node
var HoveredEndOutpost : Node
var MapCursorMovementMax : float = 0.0
var ZoomMultiplier : float = 2.0

# Inbuilt functions

func _ready() -> void:
	set_tick(GameVariables.GlobalTick)
	update_players()
	update_online_players()
	for outpost in GameVariables.OutpostData:
		set_outpost_data(outpost)
	for submarine in GameVariables.SubmarineData:
		set_submarine_data(submarine)

func _process(delta : float) -> void:
	if $Interface/Panels/Communications.visible == false:
		if Input.is_action_just_released("ZoomIn") or Input.is_action_just_released("ZoomOut"):
			var ZoomChange : float = 0
			if Input.is_action_just_released("ZoomIn"):
				ZoomChange = -(ZoomMultiplier * delta)
			elif Input.is_action_just_released("ZoomOut"):
				ZoomChange = ZoomMultiplier * delta
			$Camera/Camera2D.zoom = Vector2(clamp($Camera/Camera2D.zoom.x + ZoomChange, 0.1, 10), clamp($Camera/Camera2D.zoom.y + ZoomChange, 0.1, 10))
	if $MapControls/MapButton.pressed:
		$Camera.global_position += OldMapCursorPosition - get_global_mouse_position()
		var MapCursorMovementDifference : float = OldMapCursorPosition.distance_to(get_global_mouse_position())
		if MapCursorMovementDifference > MapCursorMovementMax:
			MapCursorMovementMax = MapCursorMovementDifference
	HoveredEndOutpost = null
	for child in $Outposts.get_children():
		if child != HoveredStartOutpost:
			if get_global_mouse_position().distance_to(child.InitialPosition) <= DistanceToHoverOutpost:
				HoveredEndOutpost = child
				child.focus()
				CursorPosition = child.InitialPosition
			elif child.Focused:
				child.unfocus()
	if HoveredEndOutpost == null:
		CursorPosition = get_global_mouse_position()
	if Input.is_action_just_released("Click"):
		create_submarine()
		$Lines/RulerLine.hide()
		$Lines/RulerLabel.hide()
		HoveredStartOutpost = null

# Tick functions

func set_tick(tick : int) -> void:
	GameVariables.VisibleTick = tick
	for child in $Submarines.get_children():
		child.update_tick()
	for child in $Outposts.get_children():
		child.update_tick()
	for child in $Interface/Panels.get_children():
		if child.has_method("update_tick"):
			child.update_tick()

func set_global_tick(globaltick : int) -> void:
	print("Set global tick to " + str(globaltick))
	var OldVisibleTickMax : int = GameVariables.VisibleTickMax
	GameVariables.GlobalTick = globaltick
	GameVariables.VisibleTickMax = globaltick + Settings.VisibleTickMaxAhead
	GenerationFunctions.set_history_expansion(OldVisibleTickMax, globaltick + Settings.VisibleTickMaxAhead)
	if GameVariables.VisibleTick + 1 == GameVariables.GlobalTick and Jumping == false:
		set_tick(GameVariables.GlobalTick)

func jump_to_tick(tick : int) -> void:
	Jumping = true
	$JumpTween.interpolate_property(GameVariables, "VisibleTick", GameVariables.VisibleTick, min(tick, GameVariables.VisibleTickMax), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$JumpTween.start()
	show_panel("Time")

func unpause_launch_timers() -> void:
	for child in $Submarines.get_children():
		child.get_node("LaunchTimer").paused = false

# Data functions

func set_outpost_data(value : Dictionary) -> void:
	var ExistingOutpost : Node = $Outposts.get_node_or_null(value["OutpostName"])
	if ExistingOutpost != null:
		set_node_variables(ExistingOutpost, value)
		return
	var OutpostInstance : Node
	var CurrentType : String = value["History"][GameVariables.VisibleTick]["CurrentType"]
	match CurrentType:
		"Factory":
			OutpostInstance = Factory.instance()
		"Generator":
			OutpostInstance = Generator.instance()
		_:
			# Failure case
			print("\"" + CurrentType + "\" is not a valid outpost type")
			return
	OutpostInstance.name = value["OutpostName"]
	set_node_variables(OutpostInstance, value)
	$Outposts.add_child(OutpostInstance)

func set_submarine_data(value : Dictionary) -> void:
	var ExistingSubmarine : Node = $Submarines.get_node_or_null(value["SubmarineName"])
	if ExistingSubmarine != null:
		set_node_variables(ExistingSubmarine, value)
		return
	var SubmarineInstance : Node = Submarine.instance()
	SubmarineInstance.name = value["SubmarineName"]
	set_node_variables(SubmarineInstance, value)
	$Submarines.add_child(SubmarineInstance)
	SubmarineInstance.prepare()

func set_node_variables(node : Node, variables : Dictionary):
	var IgnoredVariables : Array = ["OutpostName", "SubmarineName", "LaunchTime"]
	for variable in variables:
		if IgnoredVariables.has(variable) == false:
			if node.get(variable) != null:
				node.set(variable, variables[variable])
			else:
				# Failure case
				print("\"" + variable + "\" is not a valid node variable")

func create_submarine() -> void:
	if GameVariables.VisibleTick == GameVariables.GlobalTick:
		if HoveredStartOutpost != null and HoveredEndOutpost != null:
			# TODO: make work with submarines
			var HoveredStartOutpostHistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, HoveredStartOutpost.History)
			if HoveredStartOutpostHistoryEntry["CurrentPlayer"] == VariableFunctions.get_username_player(Settings.Username):
				for submarine in HoveredStartOutpost.Submarines:
					if submarine != "Canceled":
						var SubmarineNode : Node = $Submarines.get_node(submarine)
						var TickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, SubmarineNode.History)
						# TODO: make work with submarine
						if TickHistory != -1 and SubmarineNode.History[TickHistory]["Target"] == HoveredEndOutpost.name:
							if OrderFunctions.get_submarine_time_to_launch(SubmarineNode.SubmarineId) != 0:
								focus_submarine(submarine)
								return
				Network.rpc_id(
					1,
					"create_submarine",
					GameVariables.GameId,
					get_tree().get_network_unique_id(),
					GameVariables.VisibleTick,
					HoveredStartOutpost.name,
					Settings.Username,
					HoveredEndOutpost.name
				)

func cancel_submarine(submarinename : String) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarinename)
	SubmarineNode.remove()
	for panel in [$Interface/Panels/Launching, $Interface/Panels/Submarine]:
		if panel.SubmarineNode == SubmarineNode:
			panel.SubmarineNode = null
			hide_panel(panel.name)

func override_temporary_troops(submarinename : String) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarinename)
	SubmarineNode.TemporaryTroops = false
	var InitialOutpostNode : Node = $Outposts.get_node(SubmarineNode.InitialOutpost)
	InitialOutpostNode.TemporaryTroops = false
	for child in $Interface/Panels.get_children():
		if child.get("TemporaryTroops") != null:
			if child.SubmarineNode == SubmarineNode:
				child.TemporaryTroops = false

func override_temporary_gift(submarinename : String) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarinename)
	SubmarineNode.TemporaryGift = false
	SubmarineNode.update_tick()

func set_factory_panel_info(factorypanelhistory : Array) -> void:
	$Interface/Panels/Factory.History = factorypanelhistory
	$Interface/Panels/Factory.update_tick()

func set_submarine_panel_info(submarinepanelhistory : Array) -> void:
	$Interface/Panels/Submarine.History = submarinepanelhistory
	$Interface/Panels/Submarine.update_tick()

# Node functions

func get_source_node(source : String) -> Node:
	var OutpostNode : Node = $Outposts.get_node_or_null(source)
	if OutpostNode != null:
		return OutpostNode
	var SubmarineNode : Node = $Submarines.get_node_or_null(source)
	if SubmarineNode != null:
		return SubmarineNode
	# Failure case
	print("\"" + source + "\" is not a valid node")
	return Node.new()

func create_submarine_lines(submarine : String) -> Node:
	var SubmarineLinesInstance : Node = SubmarineLines.instance()
	SubmarineLinesInstance.SubmarineNode = submarine
	$Lines.add_child(SubmarineLinesInstance)
	return SubmarineLinesInstance

# Visual functions

func update_players() -> void:
	var OccupiedPlayers : Array = []
	for player in GameVariables.Players:
		if GameVariables.Players[player]["Username"] != "Dormant":
			if GameVariables.Players[player]["Occupied"] == true:
				OccupiedPlayers.append(player)
	if len(OccupiedPlayers) < len(GameVariables.Players) -1:
		$Interface/GameStartLabel.text = "Waiting for more players to join (" + str(len(OccupiedPlayers)) + "/" + str(len(GameVariables.Players) - 1) + " present)"
	else:
		$Interface/GameStartLabel.hide()
	$Interface/Panels/Communications.prepare()

func focus_outpost(outpostname : String) -> void:
	unfocus_outposts()
	unfocus_submarines()
	var OutpostNode : Node = $Outposts.get_node(outpostname)
	var OutpostHistoryEntry : Dictionary = OutpostNode.History[GameVariables.VisibleTick]
	OutpostNode.Selected = true
	$Interface/Panels/Factory.OutpostNode = OutpostNode
	$Interface/Panels/Generator.OutpostNode = OutpostNode
	switch_outpost_panel(OutpostHistoryEntry["CurrentType"])

func focus_submarine(submarinename : String) -> void:
	unfocus_outposts()
	unfocus_submarines()
	var SubmarineNode : Node = $Submarines.get_node(submarinename)
	$Interface/Panels/Launching.SubmarineNode = SubmarineNode
	$Interface/Panels/Preparing.SubmarineNode = SubmarineNode
	$Interface/Panels/Preparing.OutpostNode = $Outposts.get_node(SubmarineNode.InitialOutpost)
	$Interface/Panels/Submarine.SubmarineNode = SubmarineNode
	SubmarineNode.Selected = true
	SubmarineNode.focus()

func unfocus_outposts() -> void:
	hide_panel("Generator")
	hide_panel("Factory")
	for child in $Outposts.get_children():
		child.Selected = false
	$Lines/RulerLine.hide()
	if HoveredEndOutpost != null:
		HoveredEndOutpost.unfocus()
		HoveredEndOutpost = null

func unfocus_submarines() -> void:
	hide_panel("Submarine")
	hide_panel("Preparing")
	hide_panel("Launching")
	for child in $Submarines.get_children():
		child.unfocus()
		child.Selected = false

func show_panel(panelstring : String) -> void:
	var PanelNode : Node = $Interface/Panels.get_node(panelstring)
	if PanelNode.visible == false:
		match panelstring:
			"Communications":
				$Interface/BottomBar/HBoxContainer/CommunicationsButton.pressed = true
			"Time":
				$Interface/BottomBar/HBoxContainer/ClockButton.pressed = true
		PanelNode.show()
		if PanelNode.has_method("prepare"):
			PanelNode.prepare()

func hide_panel(panelstring : String) -> void:
	var PanelNode : Node = $Interface/Panels.get_node(panelstring)
	if PanelNode.visible:
		match panelstring:
			"Communications":
				$Interface/BottomBar/HBoxContainer/CommunicationsButton.pressed = false
			"Time":
				$Interface/BottomBar/HBoxContainer/ClockButton.pressed = false
		PanelNode.hide()

func hide_all_panels() -> void:
	for child in $Interface/Panels.get_children():
		hide_panel(child.name)

func show_popup(popupstring : String) -> void:
	var PopupNode : Node = $Interface/Popups.get_node(popupstring)
	if PopupNode.visible == false:
		match popupstring:
			"Exit":
				$Interface/BottomBar/HBoxContainer/ExitButton.pressed = true
		PopupNode.show()

func hide_popup(popupstring : String) -> void:
	var PopupNode : Node = $Interface/Popups.get_node(popupstring)
	if PopupNode.visible:
		match popupstring:
			"Exit":
				$Interface/BottomBar/HBoxContainer/ExitButton.pressed = false
		PopupNode.hide()

func hide_all_popups() -> void:
	for child in $Interface/Popups.get_children():
		hide_popup(child.name)

func switch_outpost_panel(panelstring : String) -> void:
	for panel in ["Generator", "Factory"]:
		if panel == panelstring:
			show_panel(panel)
		else:
			hide_panel(panel)

func update_online_players() -> void:
	for child in $Interface/OnlinePlayers/OnlineIndicators.get_children():
		child.queue_free()
	var DisplayOnlinePlayers : Array = GameVariables.OnlinePlayers.duplicate(true)
	DisplayOnlinePlayers.erase(VariableFunctions.get_username_player(Settings.Username))
	for i in len(DisplayOnlinePlayers):
		var OnlineIndicatorInstance = OnlineIndicator.instance()
		OnlineIndicatorInstance.rect_position.x = -25 * i
		OnlineIndicatorInstance.modulate = GameVariables.Colors.get(DisplayOnlinePlayers[i])
		$Interface/OnlinePlayers/OnlineIndicators.add_child(OnlineIndicatorInstance)
		$Interface/OnlinePlayers/OnlinePlayersLabel.rect_position.x = -90 - 25 * (i + 1)
	$Interface/OnlinePlayers/OnlinePlayersLabel.visible = not DisplayOnlinePlayers.empty()

func update_messages(chat : Array) -> void:
	if $Interface/Panels/Communications.visible:
		$Interface/Panels/Communications.update_messages(chat)

# Signals

func _on_CommunicationsButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		hide_panel("Time")
		show_panel("Communications")
	else:
		hide_panel("Communications")

func _on_ClockButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		hide_panel("Communications")
		show_panel("Time")
	else:
		hide_panel("Time")
		set_tick(GameVariables.GlobalTick)

func _on_MapButton_button_down() -> void:
	OldMapCursorPosition = get_global_mouse_position()
	OldCameraPosition = $Camera/Camera2D.global_position

func _on_MapButton_button_up() -> void:
	if MapCursorMovementMax <= MapCursorMovementAllowance:
		hide_all_panels()
		unfocus_outposts()
		unfocus_submarines()
	MapCursorMovementMax = 0
	OldMapCursorPosition = Vector2()

func _on_JumpTween_tween_step(_object : Object, _key : NodePath, _elapsed : float, value : float) -> void:
	set_tick(int(value))

func _on_JumpTween_tween_completed(_object : Object, _key : NodePath) -> void:
	Jumping = false

func _on_ExitButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		show_popup("Exit")
	else:
		hide_popup("Exit")

func _on_ExitPopupYesButton_pressed() -> void:
	Network.rpc_id(1, "remove_player_id", GameVariables.GameId, get_tree().get_network_unique_id())
	GameVariables.set_script(null)
	GameVariables.set_script(preload("res://Scripts/Global/GameVariables.gd"))
	get_tree().change_scene("res://Scenes/Menus/MainMenu.tscn")
