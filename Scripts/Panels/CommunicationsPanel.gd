extends Control

onready var ChatButton : PackedScene = preload("res://Scenes/InterfaceElements/ChatButton.tscn")
onready var PlayerPillsContainer : PackedScene = preload("res://Scenes/InterfaceElements/PlayerPillsContainer.tscn")
onready var PlayerPill : PackedScene = preload("res://Scenes/InterfaceElements/PlayerPill.tscn")
onready var Message : PackedScene = preload("res://Scenes/InterfaceElements/Message.tscn")
onready var Game : Node = get_node("/root/Game")

var CurrentPlayers : Array
var MaxScroll : int

func _ready():
	MaxScroll = $Messages/ScrollContainer.get_v_scrollbar().max_value
	$Messages/ScrollContainer.get_v_scrollbar().connect("changed", self, "jump_to_messages_end")

func prepare() -> void:
	$ExpandButton.prepare()
	$Chats.show()
	$Messages.hide()
	update_chats()

func update_chats() -> void:
	for child in $Chats/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("ChatButton"):
			child.queue_free()
	for chat in GameVariables.Chats:
		var ChatButtonInstance := ChatButton.instance()
		var Message : Dictionary = GameVariables.Chats[chat][-1].duplicate(true)
		ChatButtonInstance.Players = chat.duplicate(true)
		ChatButtonInstance.set_message(Message)
		$Chats/ScrollContainer/VBoxContainer.add_child(ChatButtonInstance)

func set_players(players : Array, playerselection : bool = false) -> void:
	var Player : String = VariableFunctions.get_username_player(Settings.Username)
	if playerselection:
		CurrentPlayers.append(Player)
		var ChatPlayers : Array
		for player in GameVariables.Players:
			if player != Player:
				var Username = GameVariables.Players[player]["Username"]
				if Username != "Dormant" and Username != "Empty":
					ChatPlayers.append(player)
		if ChatPlayers.empty() == false:
			var PlayerPills : Array
			for player in ChatPlayers:
				var PlayerPillInstance := PlayerPill.instance()
				PlayerPillInstance.set_player(player)
				PlayerPills.append(PlayerPillInstance)
			add_player_pills_to_rows(PlayerPills)
			for playerpill in PlayerPills:
				playerpill.get_node("PlayerButton").disabled = false
				playerpill.UpdateMessages = false
				playerpill.get_node("PlayerButton").pressed = false
				playerpill.UpdateMessages = true
		else:
			Game.show_popup("AbsentChatPlayers")
			$Chats.show()
			$Messages.hide()
	else:
		var PlayerPills : Array
		for player in players:
			if player != Player:
				var PlayerPillInstance := PlayerPill.instance()
				PlayerPillInstance.set_player(player)
				PlayerPills.append(PlayerPillInstance)
		add_player_pills_to_rows(PlayerPills)

func set_messages(players : Array) -> void:
	var Player : String = VariableFunctions.get_username_player(Settings.Username)
	CurrentPlayers = players
	for child in $Messages/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("Message"):
			child.queue_free()
	if GameVariables.Chats.has(players):
		for message in GameVariables.Chats[players]:
			var MessageInstance := Message.instance()
			MessageInstance.set_message(message)
			$Messages/ScrollContainer/VBoxContainer.add_child(MessageInstance)
	elif players != [Player]:
		# Failure case
		print(str(players) + " are not in an existing chat")

func set_chat(players : Array, playerselection : bool = false) -> void:
	$Chats.hide()
	$Messages.show()
	set_players(players, playerselection)
	set_messages(players)

func add_player_pills_to_rows(playerpills : Array) -> void:
	for child in $Messages/HeaderPanel/PlayerPills.get_children():
		child.queue_free()
	$Messages/HeaderPanel/PlayerPills.add_child(PlayerPillsContainer.instance()) 
	for playerpill in playerpills:
		var PlayerPillWidth : int = playerpill.get_node("PlayerLabel").get_font("font").get_string_size(playerpill.Username).x + 10
		var PlayerPillRowWidth : int = PlayerPillWidth
		for child in $Messages/HeaderPanel/PlayerPills.get_children()[-1].get_children():
			PlayerPillRowWidth += child.rect_size.x
		if PlayerPillRowWidth > $Messages/HeaderPanel/PlayerPills.rect_size.x:
			$Messages/HeaderPanel/PlayerPills.add_child(PlayerPillsContainer.instance())
		$Messages/HeaderPanel/PlayerPills.get_children()[-1].add_child(playerpill)

func update_messages(players : Array) -> void:
	if players == CurrentPlayers:
		set_messages(CurrentPlayers)
	update_chats()

func jump_to_messages_end() -> void:
	if $Messages/ScrollContainer.get_v_scrollbar().max_value != MaxScroll:
		MaxScroll = $Messages/ScrollContainer.get_v_scrollbar().max_value
		$Messages/ScrollContainer.scroll_vertical = MaxScroll

func _on_CreateChatButton_pressed() -> void:
	$Chats.hide()
	$Messages.show()
	var Player : String = VariableFunctions.get_username_player(Settings.Username)
	set_chat([Player], true)

func _on_BackButton_pressed() -> void:
	$Chats.show()
	$Messages.hide()
	update_chats()

func _on_SendButton_pressed() -> void:
	Network.rpc_id(
		1,
		"send_message",
		# Game ID
		GameVariables.GameId,
		# Message
		CurrentPlayers,
		# Chat
		{
			"Players" : [VariableFunctions.get_username_player(Settings.Username)],
			"Text" : $Messages/EditPanel/TextEdit.text
		}
	)
	set_chat(CurrentPlayers)
	$Messages/EditPanel/TextEdit.text = ""
