extends Control

onready var Game : Node = get_node("/root/Game")

export var AdjustMultiplier : float = 0.08
export var WheelChangeThreshold : int = 2
export var ArrowChangeThreshold : int = 10

var Value : float = 0.0
var OldValue : float = 0.0
var WheelDifference : float = 0.0
var OldAngle : int = 0

func _process(_delta : float) -> void:
	update_time_label()
	if $Wheel/WheelButton.pressed and Game.Jumping == false:
		$Wheel/WheelPoint.look_at(get_global_mouse_position())
		var NewWheelDifference : float = clamp(stepify($Wheel/WheelPoint.rotation_degrees - OldAngle, 1 / AdjustMultiplier), -Value, (GameVariables.VisibleTickMax / AdjustMultiplier) - Value - AdjustMultiplier)
		if abs(NewWheelDifference) >= WheelChangeThreshold:
			WheelDifference = NewWheelDifference
	var AdjustValue = Value + WheelDifference
	if AdjustValue != OldValue:
		Game.set_tick(int(AdjustValue * AdjustMultiplier))
		$Wheel/WheelSprite.rotation_degrees = AdjustValue
		OldValue = AdjustValue

func prepare() -> void:
	Value = GameVariables.VisibleTick / AdjustMultiplier
	$ExpandButton.prepare()

func update_tick() -> void:
	if Game.Jumping:
		Value = GameVariables.VisibleTick / AdjustMultiplier

func update_time_label() -> void:
	var Time : int = ((Value + WheelDifference) * AdjustMultiplier - GameVariables.GlobalTick) * GameVariables.TickLength
	if Time >= 0:
		$TimeLabel.text = VariableFunctions.get_time_text(Time)  + " from now"
	else:
		$TimeLabel.text = VariableFunctions.get_time_text(-Time)  + " ago"

func _on_WheelButton_button_down() -> void:
	$Wheel/WheelPoint.look_at(get_global_mouse_position())
	OldAngle = $Wheel/WheelPoint.rotation_degrees

func _on_WheelButton_button_up() -> void:
	if WheelDifference == 0:
		var WheelCursorMovement : float = get_global_mouse_position().x - $Wheel/WheelPoint.global_position.x
		if abs(WheelCursorMovement) > ArrowChangeThreshold:
			var NewArrowDifference = (WheelCursorMovement / abs(WheelCursorMovement)) / AdjustMultiplier
			Value = clamp(Value + NewArrowDifference, 0, GameVariables.VisibleTickMax / AdjustMultiplier - AdjustMultiplier)
	else:
		Value += WheelDifference
		WheelDifference = 0
