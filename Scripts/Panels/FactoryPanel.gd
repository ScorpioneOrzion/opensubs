extends "res://Scripts/Panels/OutpostPanel.gd"

func prepare() -> void:
	update_tick()

func get_outpost_info() -> String:
	var OutpostHistoryEntry : Dictionary = OutpostNode.History[GameVariables.VisibleTick]
	return (
		"\n+" + str(OutpostHistoryEntry["TroopNumberMultiplierTotal"] * GameVariables.GenerateTroopNumber) +
		" in " + VariableFunctions.get_time_text(get_ticks_to_production() * GameVariables.TickLength) +
		" and every " + VariableFunctions.get_time_text(OutpostHistoryEntry["TroopTimeMultiplierTotal"] * GameVariables.GenerateTroopTicks * GameVariables.TickLength)
		)

func hide_unused_buttons() -> void:
	var OutpostHistoryEntry : Dictionary = OutpostNode.History[GameVariables.VisibleTick]
	var Player : String = OutpostHistoryEntry["CurrentPlayer"]
	$ClockButton.visible = GameVariables.Players[Player]["Username"] != "Dormant"

func get_ticks_to_production() -> int:
	var TicksToProduction : int = OutpostNode.History[GameVariables.VisibleTick]["TroopTimeCountdown"]
	if TicksToProduction == 0:
		TicksToProduction = 20
	return TicksToProduction

func _on_ClockButton_pressed() -> void:
	get_node("/root/Game").jump_to_tick(GameVariables.VisibleTick + get_ticks_to_production() )
