extends Panel

onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/Launching")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/Submarine")

var OutpostNode : Node
var SubmarineNode : Node
var TemporaryTroops : bool = false

func prepare() -> void:
	$OutpostLabel.text = OutpostNode.History[GameVariables.VisibleTick]["CurrentType"].to_upper()
	update_tick()
	# TODO: look for better solution
	$TroopSelect.disconnect("value_changed", self, "_on_TroopSelect_value_changed")
	$TroopSelect.value = HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, SubmarineNode.History)["TroopTotal"]
	$TroopSelect.connect("value_changed", self, "_on_TroopSelect_value_changed")
	$TroopSelect.grab_focus()

func update_tick() -> void:
	if visible:
		$Header.modulate = VariableFunctions.get_username_color(Settings.Username)
		$TroopSelect.max_value = get_available_troops()
		if TemporaryTroops == false:
			$TroopsLabel.text = str(OutpostNode.History[GameVariables.VisibleTick]["TroopTotal"])

func update_submarine_troops() -> void:
	Network.rpc_id(1, "prepare_submarine_troops", GameVariables.GameId, SubmarineNode.SubmarineId, $TroopSelect.value)

func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$TroopsLabel.text = str(troops)

func set_temporary_outpost_troops(troops : int) -> void:
	OutpostNode.set_temporary_troops(troops)
	set_temporary_troops(troops)

func set_temporary_submarine_troops(troops : int) -> void:
	SubmarineNode.set_temporary_troops(troops)
	LaunchingPanel.set_temporary_troops(troops)

func cancel_submarine() -> void:
	$CalculateTimer.stop()
	get_node("/root/Game").hide_panel("Preparing")
	LaunchingPanel.cancel_submarine()
	SubmarinePanel.cancel_submarine()
	OutpostNode.set_temporary_troops(get_available_troops())
	SubmarineNode.cancel_submarine()
	SubmarineNode = null

func get_available_troops() -> int:
	var OutpostHistoryEntry : Dictionary = OutpostNode.History[GameVariables.VisibleTick]
	var AvailableTroops : int = OutpostHistoryEntry["TroopTotal"]
	if GameVariables.InvalidSubmarines.has(SubmarineNode.name) == false:
		AvailableTroops -= OutpostHistoryEntry["Troops"][SubmarineNode.name]
	return AvailableTroops

func _on_TroopSelect_value_changed(value : int) -> void:
	if visible:
		set_temporary_outpost_troops(get_available_troops() - value)
		set_temporary_submarine_troops(value)
		if Settings.CalculateTroopsDelay == -1:
			update_submarine_troops()
		else:
			$CalculateTimer.start(Settings.CalculateTroopsDelay)

func _on_CancelButton_pressed() -> void:
	cancel_submarine()

func _on_CalculateTimer_timeout() -> void:
	update_submarine_troops()
