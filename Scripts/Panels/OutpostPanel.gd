extends Panel

var OutpostNode : Node

func _ready() -> void:
	hide()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if OutpostNode != null:
		var OutpostHistoryEntry : Dictionary = OutpostNode.History[GameVariables.VisibleTick]
		var Player : String = OutpostHistoryEntry["CurrentPlayer"]
		$Header.modulate = GameVariables.Colors.get(Player)
		$OutpostLabel.text = OutpostHistoryEntry["CurrentType"].to_upper()
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]
		$PlayerLabel.visible = GameVariables.Players[Player]["Username"] != "Empty"
		$TroopsLabel.text = str(OutpostNode.History[GameVariables.VisibleTick]["TroopTotal"])
		$TroopsLabel.hide()
		$TroopsLabel.show()
		$InfoLabel.text = "Drillers" + get_outpost_info()
		$InfoLabel.rect_position.x = $TroopsLabel.rect_position.x + $TroopsLabel.rect_size.x + 5

func get_outpost_info() -> String:
	return ""
