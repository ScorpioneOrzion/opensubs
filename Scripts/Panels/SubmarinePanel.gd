extends Panel

var SubmarineNode : Node

func _process(_delta : float) -> void:
	# TODO: move to update tick
	if SubmarineNode != null:
		$Info/ExtraLabel.text = SubmarineNode.get_submarine_info()
		$Toasts.hide()
		$GiftButton.hide()
		if GameVariables.VisibleTick == GameVariables.GlobalTick:
			# Sets toasts visibility
			$Toasts.show()
			var VisibleToasts : Array = SubmarineNode.get_toasts()
			for child in $Toasts.get_children():
				child.visible = VisibleToasts.has(child.name)
			# Sets launching toast text
			var TimeToLaunch : int = -1
			if VisibleToasts.has("Launching"):
				TimeToLaunch = OrderFunctions.get_submarine_time_to_launch(SubmarineNode.SubmarineId)
				$Toasts/Launching/LaunchingLabel.text = "Launching in " + VariableFunctions.get_time_text(TimeToLaunch)
			# Sets gift toast text
			var TimeToGift : int = -1
			if not OrderFunctions.get_submarine_gift_order(SubmarineNode.SubmarineId).empty():
				TimeToGift = OrderFunctions.get_submarine_time_to_gift(SubmarineNode.SubmarineId)
			if VisibleToasts.has("Gifting"):
				$Toasts/Gifting/GiftingLabel.text = "Gifting in " + VariableFunctions.get_time_text(TimeToGift)
			# Sets launching toast text
			if TimeToGift != 0:
				var SubmarineTickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, SubmarineNode.History)
				if SubmarineNode.History[SubmarineTickHistory]["CurrentPlayer"] == VariableFunctions.get_username_player(Settings.Username):
					$GiftButton.show()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if visible:
		var SubmarineHistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, SubmarineNode.History)
		update_troops()
		var Player : String = SubmarineHistoryEntry["CurrentPlayer"]
		$Header.modulate = GameVariables.Colors.get(Player)
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]
		# TODO: look for better solution
		var GiftOrderPresent : bool = not OrderFunctions.get_submarine_gift_order(SubmarineNode.SubmarineId).empty()
		$GiftButton.disconnect("toggled", self, "_on_GiftButton_toggled")
		$GiftButton.pressed = GiftOrderPresent
		$GiftButton.connect("toggled", self, "_on_GiftButton_toggled")
		if GiftOrderPresent:
			$SubmarineLabel.text = "SUB (GIFT)"
		else:
			$SubmarineLabel.text = "SUB"

func update_troops() -> void:
	$Info/TroopsLabel.text = str(HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, SubmarineNode.History)["TroopTotal"])

func cancel_submarine() -> void:
	SubmarineNode = null

func _on_GiftButton_toggled(buttonpressed : bool) -> void:
	if GameVariables.VisibleTick == GameVariables.GlobalTick:
		if buttonpressed:
			SubmarineNode.set_temporary_gift()
			Network.rpc_id(1, "gift_submarine", GameVariables.GameId, GameVariables.VisibleTick, SubmarineNode.SubmarineId)
		else:
			Network.rpc_id(1, "cancel_gift_submarine", GameVariables.GameId, SubmarineNode.SubmarineId)
	else:
		$GiftButton.pressed = not OrderFunctions.get_submarine_gift_order(SubmarineNode.SubmarineId).empty()

func _on_ClockButton_pressed() -> void:
	if GameVariables.InvalidSubmarines.has(SubmarineNode.name) == false:
		SubmarineNode.jump_to_arrival()
