extends Control

onready var Game := get_node("/root/Game")

export var SelectedSizeIncrease : int = 1.1
export var OutpostCursorMovementAllowance : float = 10.0

var Selected : bool = false
var Focused : bool = false
var InitialPosition : Vector2
var History : Array = []
var OldOutpostCursorPosition : Vector2
var OutpostCursorMovementMax : float = 0.0
var Submarines : Array = []
var TemporaryTroops : bool = false

# Inbuilt functions

func _ready() -> void:
	for child in $Shield/ShieldRings.get_children():
		var ShieldSprite : Node = child.get_node("ShieldSprite")
		ShieldSprite.set_material(ShieldSprite.get_material().duplicate(true))
	$BlockMoveAnimation.play("BlockMove")
	$Labels/NameLabel.text = name
	rect_global_position = InitialPosition - rect_pivot_offset
	update_tick()

func _process(_delta : float) -> void:
	if Game.HoveredStartOutpost == self:
		var OutpostCursorMovementDifference : float = OldOutpostCursorPosition.distance_to(get_global_mouse_position())
		if OutpostCursorMovementDifference > OutpostCursorMovementMax:
			OutpostCursorMovementMax = OutpostCursorMovementDifference

# Tick functions

func update_tick() -> void:
	var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
	$Sprites/OutpostSprite.modulate = GameVariables.Colors.get(HistoryEntry["CurrentPlayer"])
	if TemporaryTroops == false:
		$Labels/TroopsLabel.text = str(HistoryEntry["TroopTotal"])
	update_shield_rings()
	if Selected:
		Game.switch_outpost_panel(HistoryEntry["CurrentType"])

# Data functions

func get_game_position(_tick : int) -> Vector2:
	return InitialPosition

# Visual functions

func focus() -> void:
	if Focused == false:
		$FocusAnimation.play("Focus")
	Focused = true

func unfocus() -> void:
	if Focused:
		$FocusAnimation.play("Unfocus")
	Focused = false

# Temporarily change troops
func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$Labels/TroopsLabel.text = str(troops)

func update_shield_rings() -> void:
	var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
	var ShieldRingNumber : int = int(ceil(HistoryEntry["ShieldMax"] / 10))
	var ShieldRing : Node
	if ShieldRingNumber > 0:
		match ShieldRingNumber:
			1:
				ShieldRing = $Shield/ShieldRings/Shield10
			2:
				ShieldRing = $Shield/ShieldRings/Shield20
			3:
				ShieldRing = $Shield/ShieldRings/Shield30
		var FillRatio : float = float(HistoryEntry["ShieldTotal"]) / float(HistoryEntry["ShieldMax"]) * (1.0 - 60.0 / 360.0) + (30.0 / 360.0)
		ShieldRing.get_node("ShieldSprite").material.set_shader_param("fill_ratio", FillRatio)
		var ShieldNotch : Node = ShieldRing.get_node("ShieldNotch")
		ShieldNotch.visible = HistoryEntry["ShieldTotal"] > 0
		var NotchRotation : float = 360 * FillRatio
		ShieldNotch.rotation_degrees = NotchRotation
		$Shield/ShieldLabel.rect_position = Vector2(115 + ((50 * $Shield/ShieldRings.scale.x) * (ShieldRingNumber - 1)), 0).rotated(deg2rad(NotchRotation)) - $Shield/ShieldLabel.rect_pivot_offset
		$Shield/ShieldLabel.text = str(HistoryEntry["ShieldTotal"])
	$Shield/ShieldLabel.visible = HistoryEntry["ShieldTotal"] > 0
	for child in $Shield/ShieldRings.get_children():
		child.visible = child == ShieldRing

# Signals

func _on_Button_button_down() -> void:
	Game.HoveredStartOutpost = self
	if Game.HoveredEndOutpost == self:
		Game.HoveredEndOutpost = null
	OldOutpostCursorPosition = get_global_mouse_position()

func _on_Button_button_up() -> void:
	if OutpostCursorMovementMax <= OutpostCursorMovementAllowance:
		Game.focus_outpost(name)
	OutpostCursorMovementMax = 0
	OldOutpostCursorPosition = Vector2()
