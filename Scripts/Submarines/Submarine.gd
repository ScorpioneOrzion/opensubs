extends Node2D

onready var Game : Node = get_node("/root/Game")
onready var PreparingPanel : Node = get_node("/root/Game/Interface/Panels/Preparing")
onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/Launching")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/Submarine")

export var OutlineWidth : int = 15

var SubmarineId : int
var History : Array = []
var InitialOutpost : String
var InitialPlayer : String
var SubmarineLines : Node
var Selected : bool = false
var AwaitingSelection : bool = false
var Canceled : bool = false
var TemporaryTroops : bool = false
var TemporaryGift : bool = false

# Inbuilt funciton

func _ready() -> void:
	$Body/Sprites.set_material($Body/Sprites.get_material().duplicate(true))

# Start functions

# Initial setup
func prepare() -> void:
	SubmarineLines = Game.create_submarine_lines(name)
	$LaunchTimer.start(OrderFunctions.get_submarine_time_to_launch(SubmarineId))
	if GameVariables.StartTime == -1:
		$LaunchTimer.stop()
	update_tick()

# Launch submarine
func launch() -> void:
	if GameVariables.VisibleTick == GameVariables.GlobalTick:
		if PreparingPanel.SubmarineNode == self:
			var HistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(GameVariables.GlobalTick, History)
			if HistoryEntry["CurrentPlayer"] == VariableFunctions.get_username_player(Settings.Username):
				Game.hide_panel("Preparing")
				Game.hide_panel("Launching")
				PreparingPanel.update_submarine_troops()
		update_tick()

# Tick functions

# Updates the submarine to show it's state at the passed tick
func update_tick() -> void:
	var TickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, History)
	var HistoryEntry : Dictionary = History[TickHistory]
	if Canceled == false and TickHistory != -1:
		$Body/Sprites.material.set_shader_param("bodycolor", Color(GameVariables.Colors.get(HistoryEntry["CurrentPlayer"])))
		var TargetNode : Node = Game.get_source_node(HistoryEntry["Target"])
		set_panel()
		show()
		SubmarineLines.set_line_points([HistoryEntry["Position"], TargetNode.get_game_position(GameVariables.VisibleTick)])
		var Launching : bool = get_launching()
		var ShowPreparing : bool = Launching or (TickHistory <= GameVariables.LaunchWaitModifier and GameVariables.VisibleTick != GameVariables.GlobalTick)
		if GameVariables.InvalidSubmarines.has(name):
			if Launching:
				SubmarineLines.switch_line("Invalid")
				if GameVariables.InvalidSubmarines[name].has("OutpostCaptured"):
					var InitialOutpostHistory : Array = Game.get_source_node(InitialOutpost).History
					if HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, InitialOutpostHistory)["CurrentPlayer"] != HistoryEntry["CurrentPlayer"]:
						hide_submarine()
			else:
				hide_submarine()
		else:
			if ShowPreparing:
				SubmarineLines.switch_line("Preparing")
			else:
				SubmarineLines.switch_line("Launched")
				$PrepareMoveAnimation.stop()
				$Body.position = Vector2()
		if ShowPreparing:
			if $PrepareMoveAnimation.is_playing() == false:
				$PrepareMoveAnimation.play("PrepareMove")
		if TemporaryTroops == false:
			$Body/TroopsLabel.text = str(HistoryEntry["TroopTotal"])
		if (History[TickHistory]["Gift"] == false and not OrderFunctions.get_submarine_gift_order(SubmarineId).empty()) or TemporaryGift:
			$GiftFlashAnimation.play("GiftFlash")
		else:
			$GiftFlashAnimation.stop()
			var Gift : bool = HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, History)["Gift"]
			$Body/Sprites/GiftSprite.visible = Gift
			$Body/Sprites/SubmarineSprite.visible = not Gift
		position = HistoryEntry["Position"]
		set_course()
	else:
		if Selected and Canceled == false:
			Selected = false
			# TODO: make work with submarine
			Game.focus_outpost(History[-1]["Target"])
		hide_submarine()

# Data functions

# Points submarine in direction of target
func set_course() -> void:
	var HistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(GameVariables.VisibleTick, History)
	var DirectionAngle : float = HistoryEntry["Position"].angle_to_point(Game.get_source_node(HistoryEntry["Target"]).get_game_position(GameVariables.VisibleTick))
	rotation = DirectionAngle - PI
	var Flipped : bool = rad2deg(DirectionAngle) < 90 and rad2deg(DirectionAngle) > -90
	if Flipped:
		$Body.scale.y = -1
		$Body/TroopsLabel.rect_scale.x = -1
	else:
		$Body.scale.y = 1
		$Body/TroopsLabel.rect_scale.x = 1

# Sends remove submarine call
func cancel_submarine() -> void:
	Canceled = true
	SubmarineLines.hide_lines()
	hide()
	print("Sending submarine cancellation to server")
	Network.rpc_id(1, "cancel_submarine", GameVariables.GameId, SubmarineId)

# Removes submarine and associated nodes
func remove() -> void:
	SubmarineLines.queue_free()
	queue_free()

# Gets the submarine's position at tick
func get_game_position(tick : int) -> Vector2:
	return HistoryFunctions.get_tick_history_entry(tick, History)["Position"]

# Jumps time controller to submarine arrival
func jump_to_arrival() -> void:
	Game.jump_to_tick(History[-1]["Tick"] + 1)

# Returns true if the submarine is currently launching
func get_launching() -> bool:
	var TickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, History)
	return TickHistory <= GameVariables.LaunchWaitModifier and OrderFunctions.get_submarine_time_to_launch(SubmarineId) != 0

# Visual functions

# Sets the correct panels to be visible
func set_panel() -> void:
	if Selected:
		var TickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, History)
		var HistoryEntry : Dictionary = History[TickHistory]
		if (
			GameVariables.VisibleTick == GameVariables.GlobalTick and
			HistoryEntry["CurrentPlayer"] == VariableFunctions.get_username_player(Settings.Username) and
			get_launching()
		):
			Game.show_panel("Preparing")
			Game.show_panel("Launching")
			Game.hide_panel("Submarine")
		else:
			Game.hide_panel("Preparing")
			Game.hide_panel("Launching")
			Game.show_panel("Submarine")

# Sets visual focused effects
func focus() -> void:
	set_panel()
	$Body/Sprites.material.set_shader_param("width", OutlineWidth)

# Sets visual unfocused effects
func unfocus() -> void:
	$Body/Sprites.material.set_shader_param("width", 0)

# Hides the submarine
func hide_submarine() -> void:
	if Selected:
		Game.hide_panel("Submarine")
		Game.hide_panel("Launching")
		Game.hide_panel("Preparing")
	SubmarineLines.hide_lines()
	hide()

# Temporarily changes troops
func set_temporary_troops(troops) -> void:
	TemporaryTroops = true
	$Body/TroopsLabel.text = str(troops)

# Temporarily changes gift sprites
func set_temporary_gift() -> void:
	TemporaryGift = true
	update_tick()

# Gets information to display on panels
func get_submarine_info() -> String:
	var Info : String = "Drillers"
	var TickHistory : int = HistoryFunctions.get_tick_history(GameVariables.VisibleTick, History)
	if TickHistory != -1:
		var TimeToArrival : int = ((History[-1]["Tick"] + 1) - GameVariables.VisibleTick) * GameVariables.TickLength
		Info += "\nArrives in " + VariableFunctions.get_time_text(TimeToArrival)
		Info += "\nSpeed multiplier " + str(History[TickHistory]["SpeedMultiplierTotal"])
	return Info

# Gets toasts that should be visible
func get_toasts() -> Array:
	var Toasts : Array
	# Checks if launch toast should be shown
	var TimeToLaunch : int = OrderFunctions.get_submarine_time_to_launch(SubmarineId)
	if TimeToLaunch != 0:
		Toasts.append("Launching")
	# Checks if gift toast should be shown
	if not OrderFunctions.get_submarine_gift_order(SubmarineId).empty():
		var TimeToGift : int = OrderFunctions.get_submarine_time_to_gift(SubmarineId)
		if TimeToGift != 0:
			Toasts.append("Gifting")
	# Checks if submarine is set to gift but will not
	if not OrderFunctions.get_submarine_gift_order(SubmarineId).empty() and History[-1]["Gift"] == false:
		Toasts.append("CannotGift")
	# Checks if error launch toasts should be shown
	if GameVariables.InvalidSubmarines.has(name):
		Toasts.append_array(GameVariables.InvalidSubmarines[name])
	return Toasts

# Signals

# Makes submarine panels visible
func _on_Button_pressed() -> void:
	Game.focus_submarine(name)

# Launches submarine
func _on_LaunchTimer_timeout() -> void:
	launch()
