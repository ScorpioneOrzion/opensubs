extends Control

onready var JoinedGameButton : PackedScene = preload("res://Scenes/InterfaceElements/JoinedGameButton.tscn")
onready var OpenGameButton : PackedScene = preload("res://Scenes/InterfaceElements/OpenGameButton.tscn")

var TickLengths : Array

func _ready() -> void:
	for child in $Menus/CreateGame/ScrollContainer/VBoxContainer.get_children():
		child.visible = not child.is_in_group("Advanced")
	if get_tree().get_network_connected_peers().empty() == false:
		Network.rpc_id(1, "get_joined_games", get_tree().get_network_unique_id(), Settings.Username)
		Network.rpc_id(1, "get_open_games", get_tree().get_network_unique_id(), Settings.Username)
		Network.rpc_id(1, "get_game_setup_options", get_tree().get_network_unique_id())

func set_game_setup_options(maxplayers : int, ticklengths : Array) -> void:
	$Menus/CreateGame/ScrollContainer/VBoxContainer/Players/PlayersSpinBox.max_value = maxplayers
	for length in ticklengths:
		TickLengths = ticklengths
		$Menus/CreateGame/ScrollContainer/VBoxContainer/TickLength/TickLengthOptionButton.add_item(VariableFunctions.get_time_text(length, true)) 

func switch_button(button : Node) -> void:
	for child in $MenuButtons.get_children():
		child.pressed = button == child

func switch_menu(menu : Node) -> void:
	for child in $Menus.get_children():
		child.visible = menu == child

func show_popup(popupstring : String) -> void:
	var PopupNode : Node = $Popups.get_node(popupstring)
	if PopupNode.visible == false:
		PopupNode.show()

func hide_popup(popupstring : String) -> void:
	var PopupNode : Node = $Popups.get_node(popupstring)
	if PopupNode.visible:
		PopupNode.hide()

func validate_game_name(gamename : String) -> bool:
	if gamename != "":
		return true
	return false

func set_joined_games(gamesinfo : Dictionary) -> void:
	for child in $Menus/JoinedGames/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("GameButton"):
			child.queue_free()
	for gameid in gamesinfo.keys():
		var JoinedGameButtonInstance = JoinedGameButton.instance()
		JoinedGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$Menus/JoinedGames/ScrollContainer/VBoxContainer.add_child(JoinedGameButtonInstance)
		$Menus/JoinedGames/ScrollContainer/VBoxContainer.move_child(JoinedGameButtonInstance, 0)

func set_open_games(gamesinfo : Dictionary) -> void:
	for child in $Menus/OpenGames/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("GameButton"):
			child.queue_free()
	for gameid in gamesinfo.keys():
		var OpenGameButtonInstance := OpenGameButton.instance()
		OpenGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$Menus/OpenGames/ScrollContainer/VBoxContainer.add_child(OpenGameButtonInstance)
		$Menus/OpenGames/ScrollContainer/VBoxContainer.move_child(OpenGameButtonInstance, 0)

func add_joined_game(gameid : int, gameinfo : Dictionary) -> void:
	var JoinedGameButtonInstance := JoinedGameButton.instance()
	JoinedGameButtonInstance.set_game(gameid, gameinfo)
	$Menus/JoinedGames/ScrollContainer/VBoxContainer.add_child(JoinedGameButtonInstance)
	$Menus/JoinedGames/ScrollContainer/VBoxContainer.move_child(JoinedGameButtonInstance, 0)

func remove_open_game(gameid : int) -> void:
	for child in $Menus/OpenGames/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("GameButton"):
			if child.GameId == gameid:
				child.queue_free()
				break

func _on_JoinedGamesButton_pressed() -> void:
	switch_button($MenuButtons/JoinedGamesButton)
	switch_menu($Menus/JoinedGames)

func _on_OpenGamesButton_pressed() -> void:
	switch_button($MenuButtons/OpenGamesButton)
	switch_menu($Menus/OpenGames)

func _on_CreateGameButton_pressed() -> void:
	switch_button($MenuButtons/CreateGameButton)
	switch_menu($Menus/CreateGame)

func _on_CreateButton_pressed() -> void:
	if validate_game_name($Menus/CreateGame/ScrollContainer/VBoxContainer/GameName/GameNameLineEdit.text) == false:
		show_popup("InvalidGameName")
	else:
		Network.rpc_id(
			1,
			"create_game", 
			get_tree().get_network_unique_id(),
			# Game name
			$Menus/CreateGame/ScrollContainer/VBoxContainer/GameName/GameNameLineEdit.text,
			# Players
			$Menus/CreateGame/ScrollContainer/VBoxContainer/Players/PlayersSpinBox.value,
			# Tick length
			TickLengths[$Menus/CreateGame/ScrollContainer/VBoxContainer/TickLength/TickLengthOptionButton.selected],
			# Launch wait modifier
			$Menus/CreateGame/ScrollContainer/VBoxContainer/LaunchWaitModifer/LaunchWaitModiferSpinBox.value,
			# Gift wait modifier
			$Menus/CreateGame/ScrollContainer/VBoxContainer/GiftWaitModifer/GiftWaitModiferSpinBox.value,
			# Username
			Settings.Username
		)
		switch_button($MenuButtons/JoinedGamesButton)
		switch_menu($Menus/JoinedGames)
		$Menus/CreateGame/ScrollContainer/VBoxContainer/GameName/GameNameLineEdit.text = ""
		$Menus/CreateGame/ScrollContainer/VBoxContainer/Players/PlayersSpinBox.value = $Menus/CreateGame/ScrollContainer/VBoxContainer/Players/PlayersSpinBox.min_value
		$Menus/CreateGame/ScrollContainer/VBoxContainer/TickLength/TickLengthOptionButton.selected = 0

func _on_SettingsMenuButton_pressed() -> void:
	get_tree().change_scene("res://Scenes/Menus/SettingsMenu.tscn")

func _on_RefreshJoinedGamesButton_pressed() -> void:
	Network.rpc_id(1, "get_joined_games", get_tree().get_network_unique_id(), Settings.Username)

func _on_RefreshOpenGamesButton_pressed() -> void:
	Network.rpc_id(1, "get_open_games", get_tree().get_network_unique_id(), Settings.Username)

func _on_ShowAdvancedButton_toggled(buttonpressed ) -> void:
	for child in $Menus/CreateGame/ScrollContainer/VBoxContainer.get_children():
		child.visible = not child.is_in_group("Advanced") or buttonpressed
