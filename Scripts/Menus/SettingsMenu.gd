extends Control

func _ready() -> void:
	$Menus/ConnectionSettings/ScrollContainer/VBoxContainer/Username/UsernameLineEdit.text = Settings.Username
	$Menus/ConnectionSettings/ScrollContainer/VBoxContainer/HostIp/HostIpLineEdit.text = Settings.HostIp
	$Menus/ConnectionSettings/ScrollContainer/VBoxContainer/HostPort/HostPortLineEdit.text = str(Settings.HostPort)
	$Menus/GameSettings/ScrollContainer/VBoxContainer/VisibleTickMaxAhead/VisibleTickMaxAheadSpinBox.value = Settings.VisibleTickMaxAhead
	$Menus/GameSettings/ScrollContainer/VBoxContainer/CalculateTroopsDelay/CalculateTroopsSpinBox.value = Settings.CalculateTroopsDelay
	$MenuButtons/MainMenuButton.disabled = not Network.ValidConnection

func switch_button(button : Node) -> void:
	for child in $MenuButtons.get_children():
		child.pressed = button == child

func switch_menu(menu : Node) -> void:
	for child in $Menus.get_children():
		child.visible = menu == child

func show_popup(popupstring : String) -> void:
	var PopupNode : Node = $Popups.get_node(popupstring)
	if PopupNode.visible == false:
		PopupNode.show()

func hide_popup(popupstring : String) -> void:
	var PopupNode : Node = $Popups.get_node(popupstring)
	if PopupNode.visible:
		match popupstring:
			"ConnectionFailed":
				enable_connection_settings()
		PopupNode.hide()

func hide_all_popups() -> void:
	for popup in $Popups.get_children():
		hide_popup(popup.name)

func validate_host_ip(hostip : String) -> bool:
	if hostip != "":
		return true
	return false

func validate_host_port(hostport : int) -> bool:
	if hostport != 0:
		return true
	return false

func validate_username(username : String) -> bool:
	var IllegalUsernames : Array = ["", "Empty", "Dormant"]
	if IllegalUsernames.has(username) == false:
		return true
	return false

func enable_main_menu() -> void:
	$MenuButtons/MainMenuButton.disabled = false
	enable_connection_settings()

func disable_main_menu() -> void:
	$MenuButtons/MainMenuButton.disabled = true
	disable_connection_settings()

func enable_connection_settings() -> void:
	$Menus/ConnectionSettings/ScrollContainer/VBoxContainer/UpdateConnectionButton.disabled = false
	$ConnectionTimer.stop()

func disable_connection_settings() -> void:
	$Menus/ConnectionSettings/ScrollContainer/VBoxContainer/UpdateConnectionButton.disabled = true

func _on_ConnectionSettingsButton_pressed() -> void:
	switch_button($MenuButtons/ConnectionSettingsButton)
	switch_menu($Menus/ConnectionSettings)

func _on_GameSettingsButton_pressed() -> void:
	switch_button($MenuButtons/GameSettingsButton)
	switch_menu($Menus/GameSettings)

func _on_MainMenuButton_pressed() -> void:
	get_tree().change_scene("res://Scenes/Menus/MainMenu.tscn")

func _on_UpdateConnectionButton_pressed() -> void:
	var HostIp : String = $Menus/ConnectionSettings/ScrollContainer/VBoxContainer/HostIp/HostIpLineEdit.text
	var HostPort : int = int($Menus/ConnectionSettings/ScrollContainer/VBoxContainer/HostPort/HostPortLineEdit.text)
	var Username : String = $Menus/ConnectionSettings/ScrollContainer/VBoxContainer/Username/UsernameLineEdit.text
	if validate_host_ip(HostIp) == false:
		show_popup("InvalidHostIp")
		print("Invalid host IP")
	elif validate_host_port(HostPort) == false:
		show_popup("InvalidHostPort")
		print("Invalid host port")
	elif validate_username(Username) == false:
		show_popup("InvalidUsername")
		print("Invalid username")
	else:
		disable_connection_settings()
		Network.ValidConnection = false
		hide_all_popups()
		if get_tree().get_network_connected_peers().empty() == false:
			Network.close_server_connection()
		Network.Config.set_value("User", "Username", Username)
		Network.Config.set_value("Connection", "HostIp", HostIp)
		Network.Config.set_value("Connection", "HostPort", HostPort)
		Network.Config.save(Network.get_config_path())
		Network.connect_to_server()
		$ConnectionTimer.start(Settings.ConnectionTimeoutLength)

func _on_ConnectionTimer_timeout() -> void:
	Network.close_server_connection()
	show_popup("ConnectionFailed")

func _on_VisibleTickMaxAheadSpinBox_value_changed(value):
	Settings.VisibleTickMaxAhead = int(value)

func _on_CalculateTroopsSpinBox_value_changed(value):
	Settings.CalculateTroopsDelay = value
