extends Button

onready var PanelHeight : float = get_parent().rect_size.y

func _process(delta):
	if pressed:
		var PanelBottom : float = get_parent().rect_position.y + get_parent().rect_size.y
		get_parent().rect_position.y = min(PanelBottom - PanelHeight, get_global_mouse_position().y)
		get_parent().rect_size.y = max(PanelBottom - get_global_mouse_position().y, PanelHeight)

func prepare() -> void:
	var PanelBottom : float = get_parent().rect_position.y + get_parent().rect_size.y
	get_parent().rect_position.y = PanelBottom - PanelHeight
	get_parent().rect_size.y = PanelHeight
