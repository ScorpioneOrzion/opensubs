extends Control

var Players : Array

func _process(delta):
	rect_min_size.y = $Message.rect_size.y + 20

func set_message(message : Dictionary) -> void:
	var OtherPlayers : Array = Players.duplicate(true)
	OtherPlayers.erase(VariableFunctions.get_username_player(Settings.Username))
	var OtherPlayersMessage : Dictionary = message.duplicate(true)
	OtherPlayersMessage["Players"] = OtherPlayers
	$Message.set_message(OtherPlayersMessage)

func _on_ChatButton_pressed():
	get_node("/root/Game/Interface/Panels/Communications/").set_chat(Players)
