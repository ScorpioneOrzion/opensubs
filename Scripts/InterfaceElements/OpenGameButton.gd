extends Control

var GameId : int

func set_game(gameid : int, game : Dictionary) -> void:
	GameId = gameid
	$NameButton.text = game["GameName"] + " - " + str(game["OccupiedPlayers"]) + "/" + str(game["MaxPlayers"]) + " players"

func _on_JoinButton_pressed() -> void:
	var MainMenu : Node = get_node("/root/MainMenu")
	MainMenu.switch_button(MainMenu.get_node("MenuButtons/JoinedGamesButton"))
	MainMenu.switch_menu(MainMenu.get_node("Menus/JoinedGames"))
	Network.rpc_id(1, "join_game", GameId, get_tree().get_network_unique_id(), Settings.Username)
