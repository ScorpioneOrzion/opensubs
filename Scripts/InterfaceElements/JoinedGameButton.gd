extends Control

var GameId : int

func set_game(gameid : int, game : Dictionary) -> void:
	GameId = gameid
	$NameButton.text = game["GameName"] + " - " + str(game["OccupiedPlayers"]) + "/" + str(game["MaxPlayers"]) + " players"

func _on_OpenButton_pressed() -> void:
	GameVariables.GameId = GameId
	Network.rpc_id(1, "open_game", GameId, get_tree().get_network_unique_id(), Settings.Username)
