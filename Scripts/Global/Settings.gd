extends Node

# User settings

var Username : String
var HostIp : String = "127.0.0.1"
var HostPort : int = 25565
var CalculateTroopsDelay : int = 0.5
var VisibleTickMaxAhead : int = 300

# Hidden settings

var MaximumSubmarineArrivalTicks : int = 1000
var ConnectionTimeoutLength : int = 3
