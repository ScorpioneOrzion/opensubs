extends Node

# Converts a number of seconds to a HH:MM:SS or HHh MMm SSs format
func get_time_text(time : int, letters : bool = false, accuracy : int = 3) -> String:
	var RemainingAccurary : int = accuracy
	var TimeText : String
	if RemainingAccurary > 0:
		# Gets text for hours
		var Hours : int = int(time) / 3600
		if Hours > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
		if Hours > 0:
			var UnitSeparator : String
			if accuracy >= 3:
				UnitSeparator = ":"
			if letters:
				UnitSeparator = "h"
			TimeText = str(Hours) + UnitSeparator
	if RemainingAccurary > 0:
		# Gets text for minutes
		var Minutes : int = (int(time) / 60) % 60
		if Minutes > 0:
			var MinutesPrefix : String
			if letters:
				if RemainingAccurary != accuracy:
					MinutesPrefix += " "
			elif Minutes <= 9:
				MinutesPrefix += "0"
			var UnitSeparator : String
			if accuracy >= 2:
				UnitSeparator = ":"
			if letters:
				UnitSeparator = "m"
			TimeText += MinutesPrefix + str(Minutes) + UnitSeparator
		elif letters == false:
			TimeText += "00:"
		if Minutes > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
	if RemainingAccurary > 0:
		# Gets text for seconds
		var Seconds : int = int(time) % 60
		if Seconds > 0:
			var SecondsPrefix : String
			if letters:
				if RemainingAccurary != accuracy:
					SecondsPrefix += " "
			elif Seconds <= 9:
				SecondsPrefix += "0"
			var UnitSeparator : String
			if letters:
				UnitSeparator = "s"
			TimeText += SecondsPrefix + str(Seconds) + UnitSeparator
		elif letters == false:
			TimeText += "00"
		elif RemainingAccurary == accuracy:
			TimeText += "Now"
		if Seconds > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
	return TimeText

# Returns the username's player
func get_username_player(username : String) -> String:
	for player in GameVariables.Players:
		if GameVariables.Players[player]["Username"] == username:
			return player
	# Failure case
	print("\"" + username + "\" is not in player list")
	return ""

# Returns the username's color
func get_username_color(username : String) -> String:
	var Player : String = get_username_player(username)
	var PlayerColor : String = GameVariables.Colors.get(Player)
	if PlayerColor != null:
		return PlayerColor
	# Failure case
	print("Could not get username color")
	return "ffffff"
