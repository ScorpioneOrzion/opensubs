extends Node

var Colors : Dictionary = {
	"Dormant" : "4C4C4C",
	"Red" : "AB2024",
	"Orange" : "D28E2A",
	"Cyan" : "5C9A8C",
	"Sky" : "7BA7D8",
	"Blue" : "3A4BA3",
	"Purple" : "6A558E",
	"Pink" : "C777B1",
	"Olive" : "686E47",
	"Beige" : "9B907B",
	"Brown" : "8B5E3B"
}

# Game settings variables

var TickLength : int
var SubmarineSpeed : int
var LaunchWaitModifier : int
var GiftWaitModifier : int
var OutpostStartingTroops : int
var GenerateTroopTicks : int
var GenerateShieldTicks : int
var GenerateTroopNumber : int
var GenerateShieldNumber : int

# Game state variables

var GameId : int
var StartTime : int = -1
var GlobalTick : int = 0
var VisibleTick : int = 0
var VisibleTickMax : int
var Orders : Array
var OutpostData : Array
var SubmarineData : Array
var InvalidSubmarines : Dictionary
var Players : Dictionary
var OnlinePlayers : Array
var Chats : Dictionary
