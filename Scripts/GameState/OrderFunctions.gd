extends Node

# Order functions

# Calculates the effect of an order
func calculate_order(order : Dictionary) -> void:
	if order["Type"] == "Launch":
		DataFunctions.calculate_submarine_data(order)
	elif order["Type"] == "Gift":
		GameState.calculate_submarine_gift(order)

# Removes and order
func remove_order(type : String, identifiername : String, identifier) -> void:
	for i in len(GameVariables.Orders):
		if GameVariables.Orders[i]["Type"] == type:
			if GameVariables.Orders[i][identifiername] == identifier:
				GameVariables.Orders.remove(i)
				break

# Retuns the launch order for the passed submarine ID
func get_submarine_launch_order(submarineid : int) -> Dictionary:
	for order in GameVariables.Orders:
		if order["Type"] == "Launch":
			if order["SubmarineId"] == submarineid:
				return order
	# Failure case
	return {}

# Retuns the gift order for the passed submarine ID
func get_submarine_gift_order(submarineid : int) -> Dictionary:
	for order in GameVariables.Orders:
		if order["Type"] == "Gift":
			if order["SubmarineId"] == submarineid:
				return order
	# Failure case
	return {}

# Returns the time until a submarine launches
func get_submarine_time_to_launch(submarineid : int) -> int:
	var LaunchTime : int = OrderFunctions.get_submarine_launch_order(submarineid)["LaunchTime"]
	var LaunchWaitTime : int = GameVariables.LaunchWaitModifier * GameVariables.TickLength
	if GameVariables.StartTime != -1:
		return int(max(LaunchWaitTime - OS.get_unix_time() + max(GameVariables.StartTime, LaunchTime), 0))
	else:
		return LaunchWaitTime

# Returns the time until a submarine gifts
func get_submarine_time_to_gift(submarineid : int) -> int:
	var GiftTime : int = OrderFunctions.get_submarine_gift_order(submarineid)["GiftTime"]
	var GiftWaitTime : int = GameVariables.GiftWaitModifier * GameVariables.TickLength
	if GameVariables.StartTime != -1:
		return int(max(GiftWaitTime - OS.get_unix_time() + max(GameVariables.StartTime, GiftTime), 0))
	else:
		return GiftWaitTime
