extends Node

# Data variables

# Calculates outpost history after arrival
func calculate_submarine_arrival(submarinename : String, submarinehistory : Array) -> void:
	var EndTick : int = submarinehistory[-1]["Tick"] + 1
	var Target : String = submarinehistory[-1]["Target"]
	var TargetData : Dictionary = DataFunctions.get_data_from_name(Target)
	var TargetHistory : Array = TargetData["History"]
	HistoryFunctions.remove_outpost_troops(EndTick, TargetHistory, submarinename)
	HistoryFunctions.remove_outpost_shield(EndTick, TargetHistory, submarinename)
	HistoryFunctions.remove_outpost_player(EndTick, TargetHistory, submarinename)
	var AttackTroops : int = submarinehistory[-1]["TroopTotal"]
	var ArrivalPlayer : String = submarinehistory[-1]["CurrentPlayer"]
	# Check if submarine will not be attacking
	var DormantOutpost : bool = GameVariables.Players[TargetHistory[EndTick]["CurrentPlayer"]]["Username"] == "Dormant"
	if TargetHistory[EndTick]["CurrentPlayer"] == ArrivalPlayer or DormantOutpost or submarinehistory[-1]["Gift"]:
		HistoryFunctions.set_outpost_troops(EndTick, TargetHistory, submarinename, AttackTroops)
		if DormantOutpost:
			HistoryFunctions.set_outpost_player(EndTick, TargetHistory, submarinename, ArrivalPlayer)
			GenerationFunctions.set_troop_generation(EndTick, Target, TargetHistory)
			GenerationFunctions.set_shield_generation(EndTick, Target, TargetHistory)
	else:
		var OverallShield : int = min(max(AttackTroops, 0), TargetHistory[EndTick]["ShieldTotal"])
		var OverallTroops : int = AttackTroops - OverallShield
		# Check if submarine will capture outpost
		if  TargetHistory[EndTick]["TroopTotal"] - OverallTroops < 0:
			var RemainingTroops : int = -(TargetHistory[EndTick]["TroopTotal"] * 2) + OverallTroops
			HistoryFunctions.set_outpost_troops(EndTick, TargetHistory, submarinename, RemainingTroops)
			HistoryFunctions.set_outpost_player(EndTick, TargetHistory, submarinename, ArrivalPlayer)
		else:
			HistoryFunctions.set_outpost_troops(EndTick, TargetHistory, submarinename, -OverallTroops)
		HistoryFunctions.set_outpost_shield(EndTick, TargetHistory, submarinename, -OverallShield)
	for submarine in TargetData["Submarines"]:
		if submarine != "Canceled":
			var Data : Dictionary = DataFunctions.get_submarine_data_from_name(submarine)
			if Data["History"][0]["Tick"] <= submarinehistory[-1]["Tick"] + GameVariables.LaunchWaitModifier:
				GameState.validate_submarine(submarine, TargetData["OutpostName"], Data["History"])

# Checks for sub-to-sub combat
func calculate_submarine_combat(submarinename : String, initialoutpost : String, submarinehistory : Array, canceled : bool = false) -> void:
	# Gets all submarines the passed submarine intersects
	var OpponentSubmarines : Array = get_potential_submarines(submarinename, initialoutpost, submarinehistory)
	var IntersectingSubmarines : Dictionary = get_intersecting_submarines(submarinename, initialoutpost, submarinehistory, OpponentSubmarines)
	# Get intersecting submarines, sorted by tick from lowest to highest
	var SortedIntersectingSubmarines : Array = []
	var IntersectingSubmarinesKeys : Array = IntersectingSubmarines.keys()
	IntersectingSubmarinesKeys.sort()
	for tick in IntersectingSubmarinesKeys:
		SortedIntersectingSubmarines.append(IntersectingSubmarines[tick])
	# Get list of intersecting submarines from most troops to least troops
	for tick in SortedIntersectingSubmarines:
		tick.sort_custom(self, "sort_data_troops")
	# Loops over all submarines this submarine intersects with
	for tick in SortedIntersectingSubmarines:
		for intersectingsubmarine in tick:
			var CombatTick : int
			for secondarytick in IntersectingSubmarines.keys():
				if IntersectingSubmarines[secondarytick].has(intersectingsubmarine):
					CombatTick = secondarytick + 1
			# Removes submarine attacks
			remove_submarine_attacked(CombatTick, submarinename, initialoutpost, submarinehistory, intersectingsubmarine["SubmarineName"])
			remove_submarine_attacked(CombatTick, intersectingsubmarine["SubmarineName"], intersectingsubmarine["InitialOutpost"], intersectingsubmarine["History"], submarinename)
			# Sets submarine attacks if not canceled
			if canceled == false:
				var HistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(CombatTick, submarinehistory).duplicate(true)
				var IntersectingHistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(CombatTick, intersectingsubmarine["History"]).duplicate(true)
				set_submarine_attacked(
					CombatTick,
					submarinename,
					initialoutpost,
					submarinehistory,
					intersectingsubmarine["SubmarineName"],
					IntersectingHistoryEntry["TroopTotal"],
					IntersectingHistoryEntry["CurrentPlayer"],
					IntersectingHistoryEntry["Gift"]
				)
				set_submarine_attacked(
					CombatTick,
					intersectingsubmarine["SubmarineName"],
					intersectingsubmarine["InitialOutpost"],
					intersectingsubmarine["History"],
					submarinename,
					HistoryEntry["TroopTotal"],
					HistoryEntry["CurrentPlayer"],
					HistoryEntry["Gift"]
				)

# Returns all submarines that could intersect with passed submarine
func get_potential_submarines(submarinename : String, initialoutpost : String, submarinehistory : Array) -> Array:
	var OpponentSubmarines : Array = []
	for submarine in GameVariables.SubmarineData:
		if submarine["SubmarineName"] != submarinename:
			if submarinehistory[0]["Tick"] >= submarine["History"][0]["Tick"] or submarinehistory[-1]["Tick"] <= submarine["History"][-1]["Tick"]:
				OpponentSubmarines.append(submarine)
	return OpponentSubmarines

# Returns submarines from passed list that will engange in sub-to-sub combat with passed submarine, grouped by tick
func get_intersecting_submarines(submarinename : String, initialoutpost : String, submarinehistory : Array, opponentsubmarines : Array) -> Dictionary:
	var IntersectingSubmarines : Dictionary
	# Loops over all submarines this submarine could intersect with
	for opponent in opponentsubmarines:
		# Loops over all history entries
		for i in len(submarinehistory) - 1:
			# Checks if opponent is still present
			if submarinehistory[i + 1]["Tick"] < opponent["History"][-1]["Tick"]:
				var CombatTick : int = submarinehistory[i]["Tick"]
				var OpponentTickHistory : int = HistoryFunctions.get_tick_history(CombatTick, opponent["History"])
				var OpponentHistoryEntry : Dictionary = opponent["History"][OpponentTickHistory]
				# TODO: consider removal
				# Checks if current tick appears in opponent's history
				if OpponentTickHistory != -1:
					# Checks if the the submarine's target is the same as this submarine's initial outpost and vice versa
					if submarinehistory[i]["Target"] == DataFunctions.get_outpost_data_from_name(opponent["InitialOutpost"])["OutpostName"] and OpponentHistoryEntry["Target"] == initialoutpost:
						# Checks if the submarines do not belong to the same player
						if submarinehistory[i]["CurrentPlayer"] != OpponentHistoryEntry["CurrentPlayer"]:
							# Gets current and next positions of submarines
							var CurrentPosition : Vector2 = submarinehistory[i]["Position"]
							var NextPostion : Vector2 = submarinehistory[i + 1]["Position"]
							var CurrentOpponentPosition : Vector2 = OpponentHistoryEntry["Position"]
							var NextOpponentPosition : Vector2 = opponent["History"][OpponentTickHistory + 1]["Position"]
							# Checks if submarine positions overlap
							if get_line_overlap([CurrentPosition, NextPostion], [CurrentOpponentPosition, NextOpponentPosition]):
								if IntersectingSubmarines.has(CombatTick):
									IntersectingSubmarines[CombatTick].append(opponent)
								else:
									IntersectingSubmarines[CombatTick] = [opponent]
								break
	return IntersectingSubmarines

# Checks if segments of a submarine path overlap
func get_line_overlap(firstline : Array, secondline : Array) -> bool:
	var FirstLineRect : Rect2 = Rect2(firstline[0], Vector2()).expand(firstline[1])
	var SecondLineRect : Rect2 = Rect2(secondline[0], Vector2()).expand(secondline[1])
	return FirstLineRect.intersects(SecondLineRect, true)

# TODO: call function less
# Calculates what happens to a submarine at arrival
func update_affected_submarines(submarinename : String, initialoutpost : String, submarinehistory : Array, canceled : bool = false) -> void:
	if submarinehistory[-1]["Tick"] <= GameVariables.VisibleTickMax:
		# Get all submarines that are affected, grouped by tick
		var UpdateSubmarines : Dictionary
		for updatesubmarine in GameVariables.SubmarineData:
			# Checks if submarine is affected by change in arrival
			if updatesubmarine["History"][-1]["Target"] == submarinehistory[-1]["Target"]:
				var UpdateSubmarineEndTick : int = updatesubmarine["History"][-1]["Tick"]
				if UpdateSubmarineEndTick >= submarinehistory[-1]["Tick"]:
					if UpdateSubmarines.has(UpdateSubmarineEndTick):
						UpdateSubmarines[UpdateSubmarineEndTick].append(updatesubmarine)
					else:
						UpdateSubmarines[UpdateSubmarineEndTick] = [updatesubmarine]
			# Checks if submarine is affected by change in launch
			elif updatesubmarine["History"][-1]["Target"] == initialoutpost:
				var UpdateSubmarineEndTick : int = updatesubmarine["History"][-1]["Tick"]
				if UpdateSubmarineEndTick >= submarinehistory[0]["Tick"]:
					if UpdateSubmarines.has(UpdateSubmarineEndTick):
						UpdateSubmarines[UpdateSubmarineEndTick].append(updatesubmarine)
					else:
						UpdateSubmarines[UpdateSubmarineEndTick] = [updatesubmarine]
		# Get updated submarines, sorted by tick from lowest to highest
		var SortedUpdateSubmarines : Array = []
		var UpdateSubmarinesKeys : Array = UpdateSubmarines.keys()
		UpdateSubmarinesKeys.sort()
		for tick in UpdateSubmarinesKeys:
			SortedUpdateSubmarines.append(UpdateSubmarines[tick])
		# Get list of tied submarines from most troops to least troops
		for tick in SortedUpdateSubmarines:
			tick.sort_custom(self, "sort_data_troops")
		# Remove submarine affect on outpost
		for tick in SortedUpdateSubmarines:
			for submarine in tick:
				var EndHistoryEntry : Dictionary = submarine["History"][-1]
				var TargetHistory : Array = DataFunctions.get_data_from_name(EndHistoryEntry["Target"])["History"]
				remove_outpost_submarine(EndHistoryEntry["Tick"], EndHistoryEntry["Target"], TargetHistory, submarine["SubmarineName"])
		# Calculate submarine affect on outpost
		calculate_submarine_combat(submarinename, initialoutpost, submarinehistory, canceled)
		for tick in SortedUpdateSubmarines:
			for submarine in tick:
				calculate_submarine_arrival(submarine["SubmarineName"], submarine["History"])

func sort_data_troops(a : Dictionary, b : Dictionary) -> bool:
	return a["History"][-1]["TroopTotal"] > b["History"][-1]["TroopTotal"]

# Calculates submarine history after attack
func set_submarine_attacked(tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array, source : String, attacktroops : int, attackplayer : String, attackgift : bool) -> void:
	if attackgift == false:
		var HistoryEntry : Dictionary = HistoryFunctions.get_tick_history_entry(tick, submarinehistory)
		if HistoryEntry["Gift"] == false:
			if HistoryEntry["Troops"].has(source) == false or -attacktroops != HistoryEntry["Troops"][source]:
				var AttackTroopTotal : int = min(attacktroops, HistoryEntry["TroopTotal"])
				HistoryFunctions.set_submarine_troops(tick, submarinename, initialoutpost, submarinehistory, source, -AttackTroopTotal)
				if HistoryEntry["TroopTotal"] == 0:
					HistoryFunctions.set_submarine_player(tick, submarinehistory, source, attackplayer)
		else:
			HistoryFunctions.set_submarine_player(tick, submarinehistory, source, attackplayer)

# Calculates submarine history after removal of attack
func remove_submarine_attacked(tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array, source : String) -> void:
	HistoryFunctions.remove_submarine_troops(tick, submarinehistory, source)
	HistoryFunctions.remove_submarine_player(tick, submarinehistory, source)

# Removes outpost submarine changes
func remove_outpost_submarine(tick : int, outpostname : String, outposthistory : Array, source : String) -> void:
	HistoryFunctions.remove_outpost_troops(tick, outposthistory, source)
	HistoryFunctions.remove_outpost_shield(tick, outposthistory, source)
	HistoryFunctions.remove_outpost_player(tick, outposthistory, source)
