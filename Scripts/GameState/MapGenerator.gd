extends Node

class_name MapGenerator

var PoissonDiscSampleLimit = 30
var NameList : Array = []

func generate_map(namegeneration : Dictionary, positiongeneration : Dictionary) -> Array:
	seed(GameVariables.GameId)
	var Names : Array = []
	match namegeneration["Method"]:
		"Classics":
			var NamesList := preload("res://Names/Classics.gd").new()
			Names = NamesList.Names
		"Capitals":
			var NamesList := preload("res://Names/Capitals.gd").new()
			Names = NamesList.Names
	var Points : Array = []
	match positiongeneration["Method"]:
		"Grid":
			Points = generate_grid_points(positiongeneration["Spacing"], positiongeneration["Size"])
		"PoissonDisc":
			Points = generate_poisson_disc_points(positiongeneration["Spacing"], positiongeneration["Size"])
	var OutpostData : Array = []
	while Points.empty() == false:
		if Names.empty():
			break
		var NameIndex : int = rand_range(0, len(Names))
		var Name : String = Names[NameIndex]
		Names.remove(NameIndex)
		var Player : String = GameVariables.Players.keys()[rand_range(0, len(GameVariables.Players.keys()))]
		var PointsIndex = rand_range(0, len(Points))
		var Position : Vector2 = Points[PointsIndex] - positiongeneration["Size"] / 2
		var Troops : int
		if GameVariables.Players[Player]["Username"] == "Dormant":
			Troops = 0
		else:
			Troops = GameVariables.OutpostStartingTroops
		var Data : Dictionary = {
			"OutpostName" : Name,
			"InitialPlayer" : Player,
			"InitialPosition" : Position,
			"InitialType" : "Factory",
			"InitialTroops" : Troops,
			"InitialShieldMax" : [0, 10, 20, 30][rand_range(0, 4)],
			"InitialShield" : 0,
			"Submarines" : []
		}
		OutpostData.append(Data)
		Points.remove(PointsIndex)
	return OutpostData

func generate_grid_points(spacing : int, size : Vector2) -> Array:
	var Points : Array = []
	for i in int(size.x / spacing):
		for secondaryi in int(size.y / spacing):
			Points.append(Vector2(i * spacing, secondaryi * spacing))
	return Points

func generate_poisson_disc_points(spacing : int, size : Vector2, edgespacing : float = 0) -> Array:
	var Grid : Array = []
	var Points : Array = []
	var SpawnPoints : Array = []
	var CellSize : int = int(spacing / sqrt(2))
	for x in int(size.x / CellSize) + 1:
		Grid.append([])
		for y in int(size.y / CellSize) + 1:
			Grid[x].append(-1)
	var FirstPoint : Vector2 = Vector2(rand_range(0, size.x), rand_range(0, size.y))
	Points.append(FirstPoint)
	SpawnPoints.append(FirstPoint)
	var GridPosition : Vector2 = Vector2(int(SpawnPoints[0].x / CellSize), int(SpawnPoints[0].y / CellSize) )
	Grid[GridPosition.x][GridPosition.y] = 0
	var DistanceFromEdge : float = spacing * edgespacing
	var ValidRect : Rect2 = Rect2(Vector2(DistanceFromEdge, DistanceFromEdge), size - Vector2(DistanceFromEdge, DistanceFromEdge) * 2)
	while SpawnPoints.empty() == false: 
		var CandidateAccepted : bool = false
		var SpawnPointsIndex : int = rand_range(0, len(SpawnPoints) - 1)
		for i in PoissonDiscSampleLimit:
			var Angle : float = rand_range(0, PI * 2)
			var Distance : Vector2 = Vector2(rand_range(spacing, spacing * 2), 0) 
			var CandidatePoint : Vector2 = SpawnPoints[SpawnPointsIndex] + Distance.rotated(Angle)
			if ValidRect.has_point(CandidatePoint):   
				GridPosition = Vector2(int(CandidatePoint.x / CellSize), int(CandidatePoint.y / CellSize))
				if Grid[GridPosition.x][GridPosition.y] == -1:
					var NearbyPoints : Array = []
					for x in 5:
						for y in 5:
							var SearchPoint : Vector2 = Vector2(GridPosition.x + x - 2, GridPosition.y + y - 2)
							if SearchPoint.x < 0 or SearchPoint.x > len(Grid) -1 or SearchPoint.y < 0 or SearchPoint.y > len(Grid[0]) -1:
								continue
							if Grid[SearchPoint.x][SearchPoint.y] != -1:
								NearbyPoints.append(Grid[SearchPoint.x][SearchPoint.y])
					var SkipPoint : bool = false
					for secondaryi in NearbyPoints:
						var DistanceToCandidate : float = Points[secondaryi].distance_to(CandidatePoint)
						if DistanceToCandidate < spacing:
							SkipPoint = true
							break
					if SkipPoint:
						continue
					SpawnPoints.append(CandidatePoint)
					Points.append(CandidatePoint)
					Grid[GridPosition.x][GridPosition.y] = len(Points) - 1 
					CandidateAccepted = true
				else:
					continue
		if CandidateAccepted == false:
			SpawnPoints.remove(SpawnPointsIndex)
	return Points
