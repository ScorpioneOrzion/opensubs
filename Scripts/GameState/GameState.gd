extends Node

var MapGenerator = load("res://Scripts/GameState/MapGenerator.gd").new()

var FocusNextSubmarine : bool = false

# Game state functions

# Calculates the current game state based on all orders given
func calculate_game_state(orders : Array) -> void:
	var Outposts = MapGenerator.generate_map({"Method" : "Classics"}, {"Method" : "PoissonDisc", "Spacing" : 300, "Size" : Vector2(2000, 2000)})
	GameVariables.VisibleTickMax = GameVariables.GlobalTick + Settings.VisibleTickMaxAhead
	for outpost in Outposts:
		DataFunctions.calculate_outpost_data(outpost)
	for order in orders:
		OrderFunctions.calculate_order(order)
	get_tree().change_scene("res://Scenes/Game/Game.tscn")

# Data functions

# Cancels a submarine
func cancel_submarine(submarinename : String) -> void:
	var Data : Dictionary = DataFunctions.get_submarine_data_from_name(submarinename)
	var History : Array = Data["History"].duplicate(true)
	var InitialOutpost : String = Data["InitialOutpost"]
	var InitialOutpostData = DataFunctions.get_outpost_data_from_name(InitialOutpost)
	# TODO: check if target is outpost or submarine
	var EndTargetData = DataFunctions.get_outpost_data_from_name(History[-1]["Target"])
	CombatFunctions.remove_outpost_submarine(History[0]["Tick"], InitialOutpostData["OutpostName"], InitialOutpostData["History"], submarinename)
	CombatFunctions.remove_outpost_submarine(History[0]["Tick"], EndTargetData["OutpostName"], EndTargetData["History"], submarinename)
	InitialOutpostData["Submarines"].erase(submarinename)
	InitialOutpostData["Submarines"].append("Canceled")
	var OutpostNode : Node = get_node_or_null("/root/Game/Outposts/" + InitialOutpost)
	# TODO: consider removal of outpost submarines variable
	if OutpostNode != null:
		OutpostNode.Submarines = InitialOutpostData["Submarines"]
	OrderFunctions.remove_order("Launch", "SubmarineId", Data["SubmarineId"])
	for i in len(GameVariables.SubmarineData):
		if GameVariables.SubmarineData[i]["SubmarineName"] == submarinename:
			GameVariables.SubmarineData.remove(i)
			break
	CombatFunctions.update_affected_submarines(submarinename, InitialOutpost, History, true)
	# TODO: move to set_troops function
	get_node("/root/Game/Outposts/" + InitialOutpost).update_tick()

# Calculates the effect of a submarine gift
func calculate_submarine_gift(order : Dictionary) -> void:
	var Data : Dictionary = DataFunctions.get_submarine_data_from_id(order["SubmarineId"])
	var SubmarineName : String = Data["SubmarineName"]
	if order["Tick"] + GameVariables.GiftWaitModifier <= Data["History"][-1]["Tick"]:
		HistoryFunctions.set_submarine_gift(order["Tick"] + GameVariables.GiftWaitModifier, Data["History"], true)
		CombatFunctions.update_affected_submarines(SubmarineName, Data["InitialOutpost"], Data["History"])
		if get_tree().current_scene.name == "Game":
			get_node("/root/Game").override_temporary_gift(SubmarineName)

# Cancels a submarine gift
func cancel_gift_submarine(submarinename : String) -> void:
	var Data : Dictionary = DataFunctions.get_submarine_data_from_name(submarinename)
	HistoryFunctions.set_submarine_gift(Data["History"][0]["Tick"], Data["History"], false)
	CombatFunctions.update_affected_submarines(submarinename, Data["InitialOutpost"], Data["History"])
	OrderFunctions.remove_order("Gift", "SubmarineId", Data["SubmarineId"])
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").override_temporary_gift(submarinename)

# Sets a submarine's positions
func calculate_submarine_positions(tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array) -> void:
	var TickHistory : int = HistoryFunctions.get_tick_history(tick, submarinehistory)
	var StartHistoryEntry : Dictionary = submarinehistory[TickHistory].duplicate(true)
	for i in Settings.MaximumSubmarineArrivalTicks:
		var ITickHistory : int = TickHistory + i
		# Addes new history entry if needed
		if ITickHistory >= len(submarinehistory):
			var NewHistoryEntry : Dictionary = StartHistoryEntry.duplicate(true)
			NewHistoryEntry["Tick"] = StartHistoryEntry["Tick"] + i
			submarinehistory.append(NewHistoryEntry)
		var HistoryEntry : Dictionary = submarinehistory[ITickHistory]
		# Gets the previous position
		var OldPosition : Vector2 = submarinehistory[0]["Position"]
		if ITickHistory != 0:
			OldPosition = submarinehistory[ITickHistory - 1]["Position"]
		# Checks if tick is after submarine launch
		if ITickHistory > GameVariables.LaunchWaitModifier:
			# Gets information about current position
			var CurrentTargetPosition : Vector2 = get_game_position(HistoryEntry["Tick"], HistoryEntry["Target"])
			var CurrentDirectionAngle : float = OldPosition.angle_to_point(CurrentTargetPosition)
			var CurrentPosition : Vector2 = OldPosition + Vector2(GameVariables.SubmarineSpeed * HistoryEntry["SpeedMultiplierTotal"], 0).rotated(CurrentDirectionAngle + PI)
			# Gets information about next position
			var NextTargetPosition : Vector2 = get_game_position(HistoryEntry["Tick"], submarinehistory[ITickHistory]["Target"])
			var NextDirectionAngle : float = CurrentPosition.angle_to_point(NextTargetPosition)
			var NextPosition : Vector2 = CurrentPosition + Vector2(GameVariables.SubmarineSpeed * submarinehistory[ITickHistory]["SpeedMultiplierTotal"], 0).rotated(NextDirectionAngle + PI)
			HistoryEntry["Position"] = CurrentPosition
			# Checks if submarien will arrive at target
			if Rect2(CurrentPosition, Vector2()).expand(NextPosition).intersects(Rect2(CurrentTargetPosition, Vector2()), true):
				# TODO: replace loop
				# Removes unneeded history entries 
				var ToRemove : Array = []
				for secondaryi in len(submarinehistory):
					if submarinehistory[secondaryi]["Tick"] >= HistoryEntry["Tick"]:
						ToRemove.append(i)
				for historyentry in ToRemove:
					submarinehistory.remove(i)
				break
		else:
			HistoryEntry["Position"] = OldPosition
	CombatFunctions.update_affected_submarines(submarinename, initialoutpost, submarinehistory)

# Calculates if a submarine is valid
func validate_submarine(submarinename : String, initialoutpost : String, submarinehistory : Array) -> void:
	var InvalidReasons : Array
	# Checks if there are too many or too few troops
	var InitialOutpostHistory : Array = DataFunctions.get_outpost_data_from_name(initialoutpost)["History"]
	var InitialOutpostHistoryEntry : Dictionary = InitialOutpostHistory[submarinehistory[0]["Tick"] + GameVariables.LaunchWaitModifier]
	var InitialOutpostAvailableTroops : int = InitialOutpostHistoryEntry["TroopTotal"]
	if GameVariables.InvalidSubmarines.has(submarinename) == false:
		if InitialOutpostHistoryEntry["Troops"].has(submarinename):
			InitialOutpostAvailableTroops -= InitialOutpostHistoryEntry["Troops"][submarinename]
	if submarinehistory[0]["Troops"][initialoutpost] == 0:
		InvalidReasons.append("AbsentTroops")
	elif submarinehistory[0]["Troops"][initialoutpost] > InitialOutpostAvailableTroops:
		InvalidReasons.append("ExcessTroops")
	# Checks if the outpost is captured before submarine is launched
	var OutpostCaptured : bool = false
	for i in GameVariables.LaunchWaitModifier:
		if InitialOutpostHistory[submarinehistory[0]["Tick"] + i]["CurrentPlayer"] != submarinehistory[0]["CurrentPlayer"]:
			OutpostCaptured = true
			break
	if OutpostCaptured:
		InvalidReasons.append("OutpostCaptured")
	# Adds or removes submarine from invalid submarines list
	if InvalidReasons.empty():
		GameVariables.InvalidSubmarines.erase(submarinename)
	else:
		GameVariables.InvalidSubmarines[submarinename] = InvalidReasons
	HistoryFunctions.calculate_history_total(submarinehistory[0]["Tick"], InitialOutpostHistory, "Troops", "TroopTotal")
	var SubmarineNode : Node = get_node_or_null("/root/Game/Submarines/" + submarinename)
	if SubmarineNode != null:
		SubmarineNode.update_tick()

# Changes submarine troops whilst preparing
func prepare_submarine_troops(submarinename : String, troops : int) -> void:
	var Data : Dictionary = DataFunctions.get_submarine_data_from_name(submarinename)
	var InitialOutpost : String = Data["InitialOutpost"]
	var History : Array = Data["History"]
	var StartTick : int = History[0]["Tick"]
	var InitialOutpostHistory : Array = DataFunctions.get_outpost_data_from_name(InitialOutpost)["History"]
	HistoryFunctions.set_submarine_troops(StartTick, submarinename, InitialOutpost, History, InitialOutpost, troops)
	HistoryFunctions.set_outpost_troops(StartTick, InitialOutpostHistory, submarinename, -troops)
	CombatFunctions.update_affected_submarines(submarinename, InitialOutpost, History)
	get_node("/root/Game/Outposts/" + InitialOutpost).update_tick()

# Variable functions

# Returns the name for a new submarine launched from passed outpost
func get_new_submarine_name(outpostname : String) -> String:
	var Data : Dictionary = DataFunctions.get_outpost_data_from_name(outpostname)
	var SubmarineName : String = str(Data["OutpostName"]) + "-" + str(len(Data["Submarines"]))
	Data["Submarines"].append(SubmarineName)
	var OutpostNode : Node = get_node_or_null("/root/Game/Outposts/" + outpostname)
	if OutpostNode != null:
		OutpostNode.Submarines = Data["Submarines"]
	return SubmarineName

# Gets the passed item's position at passed tick
func get_game_position(tick : int, itemname : String) -> Vector2:
	var Data : Dictionary = DataFunctions.get_data_from_name(itemname)
	if Data.has("SubmarineName"):
		return HistoryFunctions.get_tick_history_entry(tick, Data["History"])["Position"]
	elif Data.has("OutpostName"):
		return Data["InitialPosition"]
	# Failure case
	print("\"" + itemname + "\" is not a submarine or outpost")
	return Vector2()
