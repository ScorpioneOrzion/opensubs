extends Node

# Tick functions

# Returns the index in history for passed tick
func get_tick_history(tick : int, history : Array) -> int:
	for i in len(history):
		if history[i]["Tick"] == tick:
			return i
	# Failure case
	return -1

# Returns the entry in history for passed tick
func get_tick_history_entry(tick : int, history : Array) -> Dictionary:
	var TickHistory = get_tick_history(tick, history)
	if TickHistory != -1:
		return history[TickHistory]
	else:
		# Failure case
		return {}

# Variable functions

# Sets the history collection dictionary entry for source and calculates new total
func set_history_totals(tick : int, itemhistory : Array, collection : String, total : String, source : String, value : int, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][collection][source] = value
	calculate_history_total(tick, itemhistory, collection, total, submarine)

# Remove the history collection dictionary entry for source and calculates new total
func remove_history_totals(tick : int, itemhistory : Array, collection : String, total : String, source : String, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][collection].erase(source)
	calculate_history_total(tick, itemhistory, collection, total, submarine)

# Calculates new total of history collection
func calculate_history_total(tick : int, itemhistory : Array, collection : String, total : String, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		var NewCollectionTotal : int = 0
		for collectionentry in itemhistory[ITickHistory][collection]:
			if GameVariables.InvalidSubmarines.has(collectionentry) == false:
				NewCollectionTotal += itemhistory[ITickHistory][collection][collectionentry]
		itemhistory[ITickHistory][total] = NewCollectionTotal

# Sets the history list array entry for source and calculates new current value
func set_history_current(tick : int, itemhistory : Array, list : String, current : String, source : String, value : String, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][list].insert(0, {"Source" : source, "Value" : value})
	calculate_history_current(tick, itemhistory, list, current, submarine)

# Removes the history list array entry for source and calculates new current value
func remove_history_current(tick : int, itemhistory : Array, list : String, current : String, source : String, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		for secondaryi in len(itemhistory[ITickHistory][list]):
			if itemhistory[ITickHistory][list][secondaryi]["Source"] == source:
				itemhistory[ITickHistory][list].remove(secondaryi)
				break
	calculate_history_current(tick, itemhistory, list, current, submarine)

# Calculates new value of history list
func calculate_history_current(tick : int, itemhistory : Array, list : String, current : String, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		for listentry in itemhistory[ITickHistory][list]:
			if GameVariables.InvalidSubmarines.has(listentry["Source"]) == false:
				itemhistory[ITickHistory][current] = listentry["Value"]
				break

# Sets the history boolean
func set_history_boolean(tick : int, itemhistory : Array, boolean : String, value : bool, submarine : bool = false) -> void:
	var TickHistory : int = tick
	if submarine:
		TickHistory = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		itemhistory[TickHistory + i][boolean] = value

# Sets outpost troops
func set_outpost_troops(tick : int, outposthistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, outposthistory, "Troops", "TroopTotal", source, value)

# Removes outpost troops
func remove_outpost_troops(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_totals(tick, outposthistory, "Troops", "TroopTotal", source)

# Sets outpost shield
func set_outpost_shield(tick : int, outposthistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, outposthistory, "Shield", "ShieldTotal", source, value)

# Removes outpost shield
func remove_outpost_shield(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_totals(tick, outposthistory, "Shield", "ShieldTotal", source)

# Sets outpost player
func set_outpost_player(tick : int, outposthistory : Array, source : String, value : String) -> void:
	set_history_current(tick, outposthistory, "Players", "CurrentPlayer", source, value)

# Removes outpost player
func remove_outpost_player(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_current(tick, outposthistory, "Players", "CurrentPlayer", source)

# Sets submarine troops
func set_submarine_troops(tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, submarinehistory, "Troops", "TroopTotal", source, value, true)
	GameState.validate_submarine(submarinename, initialoutpost, submarinehistory)

# Removes submarine troops
func remove_submarine_troops(tick : int, submarinehistory : Array, source : String) -> void:
	remove_history_totals(tick, submarinehistory, "Troops", "TroopTotal", source, true)

# Sets submarine player
func set_submarine_player(tick : int, submarinehistory : Array, source : String, value : String) -> void:
	set_history_current(tick, submarinehistory, "Players", "CurrentPlayer", source, value, true)

# Removes submarine player
func remove_submarine_player(tick : int, submarinehistory : Array, source : String) -> void:
	remove_history_current(tick, submarinehistory, "Players", "CurrentPlayer", source, true)

# Sets submarine gift
func set_submarine_gift(tick : int, submarinehistory : Array, value : bool) -> void:
	set_history_boolean(tick, submarinehistory, "Gift", value, true)
