extends Node

# Data functions

# Expands histories
func set_history_expansion(starttick : int, endtick : int) -> void:
	for outpost in GameVariables.OutpostData:
		set_outpost_history_expansion(starttick, endtick, outpost["OutpostName"], outpost["History"])
	for submarine in GameVariables.SubmarineData:
		set_submarine_history_expansion(endtick, submarine["SubmarineName"], submarine["History"])

# Expands outpost history
func set_outpost_history_expansion(starttick : int, endtick : int, outpostname : String, outposthistory : Array) -> void:
	var StartHistoryEntry : Dictionary = outposthistory[-1]
	for i in endtick - StartHistoryEntry["Tick"]:
		var ITickHistory : int = StartHistoryEntry["Tick"] + i + 1
		var NewHistoryEntry : Dictionary = StartHistoryEntry.duplicate(true)
		NewHistoryEntry["Tick"] = ITickHistory
		outposthistory.append(NewHistoryEntry)
	set_shield_generation(starttick, outpostname, outposthistory)
	set_troop_generation(starttick, outpostname, outposthistory)

# Expands submarine history
func set_submarine_history_expansion(endtick : int, submarinename : String, submarinehistory : Array) -> void:
	if endtick <= submarinehistory[-1]["Tick"]:
		var InitialOutpost : String = DataFunctions.get_outpost_data_from_name(submarinename)["InitialOutpost"]
		CombatFunctions.calculate_submarine_combat(submarinename, InitialOutpost, submarinehistory)
		CombatFunctions.update_affected_submarines(submarinename, InitialOutpost, submarinehistory)

# Sets outpost shield generation
func set_shield_generation(tick : int, outpostname : String, outposthistory : Array) -> void:
	HistoryFunctions.set_outpost_shield(tick, outposthistory, outpostname, outposthistory[tick]["Shield"][outpostname])
	for i in GameVariables.VisibleTickMax - tick:
		var ITickHistory : int = tick + i 
		var HistoryEntry = outposthistory[ITickHistory]
		var CurrentGenerateShieldTicks : int = int(ceil(GameVariables.GenerateShieldTicks * HistoryEntry["ShieldTimeMultiplierTotal"]))
		var CurrentShieldTimeCountdown : int
		if GameVariables.Players[HistoryEntry["CurrentPlayer"]]["Username"] == "Dormant":
			CurrentShieldTimeCountdown = CurrentGenerateShieldTicks
		else:
			var LoweredShieldTimeCountdown = CurrentGenerateShieldTicks
			if ITickHistory != 0:
				LoweredShieldTimeCountdown = outposthistory[ITickHistory - 1]["ShieldTimeCountdown"] - 1
			CurrentShieldTimeCountdown = LoweredShieldTimeCountdown + CurrentGenerateShieldTicks * int(LoweredShieldTimeCountdown < 0)
		HistoryEntry["ShieldTimeCountdown"] = CurrentShieldTimeCountdown
		if CurrentShieldTimeCountdown == 0:
			var ShieldIncrease : int = GameVariables.GenerateShieldNumber * HistoryEntry["ShieldNumberMultiplierTotal"]
			for secondaryi in len(outposthistory) - ITickHistory:
				var SecondaryITickHistory : int = ITickHistory + secondaryi
				var MaximumShieldIncrease : int = min(ShieldIncrease, outposthistory[SecondaryITickHistory]["ShieldMax"] - outposthistory[SecondaryITickHistory]["ShieldTotal"])
				outposthistory[SecondaryITickHistory]["Shield"][outpostname] += MaximumShieldIncrease
				outposthistory[SecondaryITickHistory]["ShieldTotal"] += MaximumShieldIncrease

# Sets outpost troop generation
func set_troop_generation(tick : int, outpostname : String, outposthistory : Array) -> void:
	HistoryFunctions.set_outpost_troops(tick, outposthistory, outpostname, outposthistory[tick]["Troops"][outpostname])
	for i in GameVariables.VisibleTickMax - tick:
		var ITickHistory : int = tick + i
		var HistoryEntry = outposthistory[ITickHistory]
		if HistoryEntry["CurrentType"] == "Factory":
			var CurrentGenerateTroopTicks : int = int(ceil(GameVariables.GenerateTroopTicks * HistoryEntry["TroopTimeMultiplierTotal"]))
			var CurrentTroopTimeCountdown : int
			if GameVariables.Players[HistoryEntry["CurrentPlayer"]]["Username"] == "Dormant":
				CurrentTroopTimeCountdown = CurrentGenerateTroopTicks
			else:
				var LoweredTroopTimeCountdown = CurrentGenerateTroopTicks
				if ITickHistory != 0:
					LoweredTroopTimeCountdown = outposthistory[ITickHistory - 1]["TroopTimeCountdown"] - 1
				CurrentTroopTimeCountdown = LoweredTroopTimeCountdown + CurrentGenerateTroopTicks * int(LoweredTroopTimeCountdown < 0)
			HistoryEntry["TroopTimeCountdown"] = CurrentTroopTimeCountdown
			if CurrentTroopTimeCountdown == 0:
				var TroopIncrease : int = GameVariables.GenerateTroopNumber * HistoryEntry["TroopNumberMultiplierTotal"]
				for secondaryi in len(outposthistory) - ITickHistory:
					var SecondaryITickHistory : int = ITickHistory + secondaryi
					outposthistory[SecondaryITickHistory]["Troops"][outpostname] += TroopIncrease
					outposthistory[SecondaryITickHistory]["TroopTotal"] += TroopIncrease
		else:
			HistoryEntry["TroopTimeCountdown"] = GameVariables.GenerateTroopTicks * HistoryEntry["TroopTimeMultiplierTotal"]
