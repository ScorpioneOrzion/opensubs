extends Node

# Variable functions

# Calculates the data for an outpost
func calculate_outpost_data(outpostinfo : Dictionary) -> void:
	var OutpostName : String = outpostinfo["OutpostName"]
	var History : Array = []
	for i in GameVariables.VisibleTickMax:
		var HistoryEntry : Dictionary = {
			"Tick" : i,
			"Troops" : {OutpostName : outpostinfo["InitialTroops"]},
			"TroopTotal" : outpostinfo["InitialTroops"],
			"TroopTimeMultipliers" : {OutpostName : 1},
			"TroopTimeMultiplierTotal" : 1,
			"TroopTimeCountdown" : GameVariables.GenerateTroopTicks,
			"TroopNumberMultipliers" : {OutpostName : GameVariables.GenerateTroopNumber},
			"TroopNumberMultiplierTotal" : GameVariables.GenerateShieldNumber,
			"Shield" : {OutpostName : outpostinfo["InitialShield"]},
			"ShieldTotal" : outpostinfo["InitialShield"],
			"ShieldTimeMultipliers" : {OutpostName : 1},
			"ShieldTimeMultiplierTotal" : 1,
			"ShieldTimeCountdown" : GameVariables.GenerateShieldTicks,
			"ShieldNumberMultipliers" : {OutpostName : GameVariables.GenerateShieldNumber},
			"ShieldNumberMultiplierTotal" : GameVariables.GenerateShieldNumber,
			"ShieldMax" : outpostinfo["InitialShieldMax"],
			"Players" : [{"Source" : OutpostName, "Value" :  outpostinfo["InitialPlayer"]}],
			"CurrentPlayer" : outpostinfo["InitialPlayer"],
			"Types" : [{"Source" : OutpostName, "Value" :  outpostinfo["InitialType"]}],
			"CurrentType" : outpostinfo["InitialType"]
		}
		History.append(HistoryEntry)
	var OutpostData : Dictionary = {
		"OutpostName" : OutpostName,
		"History" : History,
		"InitialPosition" : outpostinfo["InitialPosition"],
		"Submarines" : []
	}
	GameVariables.OutpostData.append(OutpostData)
	GenerationFunctions.set_troop_generation(0, OutpostName, History)
	GenerationFunctions.set_shield_generation(0, OutpostName, History)

# Calculates the data for an submarine
func calculate_submarine_data(order : Dictionary) -> void:
	var InitialOutpost : String = order["InitialOutpost"]
	var InitialOutpostData : Dictionary = DataFunctions.get_outpost_data_from_name(InitialOutpost)
	var SubmarineName : String = GameState.get_new_submarine_name(InitialOutpost)
	var History : Array = [{
		"Tick" : order["Tick"],
		"Position" : InitialOutpostData["InitialPosition"],
		"Target" : order["InitialTarget"],
		"Troops" : {InitialOutpost : order["InitialTroops"]},
		"TroopTotal" : order["InitialTroops"],
		"SpeedMultipliers" : {SubmarineName : 1},
		"SpeedMultiplierTotal" : 1,
		"Gift" : false,
		"Players" : [{"Source" : SubmarineName, "Value" : order["InitialPlayer"]}],
		"CurrentPlayer" : order["InitialPlayer"]
	}]
	var Data : Dictionary = {
		"SubmarineId" : order["SubmarineId"],
		"SubmarineName" : SubmarineName,
		"LaunchTime" : order["LaunchTime"],
		"History" : History,
		"InitialOutpost" : InitialOutpost
	}
	GameVariables.SubmarineData.append(Data)
	GameState.validate_submarine(SubmarineName, InitialOutpost, History)
	HistoryFunctions.set_outpost_troops(order["Tick"], InitialOutpostData["History"], SubmarineName, -order["InitialTroops"])
	GameState.calculate_submarine_positions(order["Tick"], SubmarineName, InitialOutpost, History)
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").set_submarine_data(Data)
		get_node("/root/Game/Outposts/" + InitialOutpost).update_tick()
		if GameState.FocusNextSubmarine:
			get_node("/root/Game").focus_submarine(SubmarineName)
			GameState.FocusNextSubmarine = false

# Returns the data for passed item
func get_data_from_name(itemname : String) -> Dictionary:
	var OutpostData : Dictionary = get_outpost_data_from_name(itemname)
	if not OutpostData.empty():
		return OutpostData
	var SubmarineData : Dictionary = get_submarine_data_from_name(itemname)
	if not SubmarineData.empty():
		return SubmarineData
	# Failure case
	return {}

# Returns the data for passed submarine
func get_submarine_data_from_name(submarinename : String) -> Dictionary:
	for submarine in GameVariables.SubmarineData:
		if submarine["SubmarineName"] == submarinename:
			return submarine
	# Failure case
	return {}

# Returns the data for passed outpost
func get_outpost_data_from_name(outpostname : String) -> Dictionary:
	for outpost in GameVariables.OutpostData:
		if outpost["OutpostName"] == outpostname:
			return outpost
	# Failure case
	return {}

# Returns the data for passed submarine ID
func get_submarine_data_from_id(submarineid : int) -> Dictionary:
	for submarine in GameVariables.SubmarineData:
		if submarine["SubmarineId"] == submarineid:
			return submarine
	# Failure case
	return {}

# Returns the name for passed submarine ID
func get_submarine_name_from_id(submarineid : int) -> String:
	return DataFunctions.get_submarine_data_from_id(submarineid)["SubmarineName"]
