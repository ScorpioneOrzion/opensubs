extends Line2D

onready var Game = get_node("/root/Game")

func _process(_delta : float) -> void:
	if Game.HoveredStartOutpost != null:
		show()
		points = [Game.HoveredStartOutpost.InitialPosition, Game.CursorPosition]
