extends Node2D

onready var Game = get_node("/root/Game")
onready var LineButton : PackedScene = preload("res://Scenes/Lines/LineButton.tscn")
onready var LineButtonInstance : Node
onready var Lines : Dictionary = {
	"Preparing" : $PreparingLine,
	"Launched" : $LaunchedLine,
	"Invalid" : $InvalidLine
}

var SubmarineNode : String

func _ready() -> void:
	LineButtonInstance = LineButton.instance()
	Game.get_node("LineButtons").add_child(LineButtonInstance)
	LineButtonInstance.connect("pressed", self, "line_button_pressed")

func line_button_pressed() -> void:
	Game.focus_submarine(SubmarineNode)

func set_line_points(linepoints : Array) -> void:
	for child in get_children():
		child.points = linepoints
	LineButtonInstance.rect_size.x = linepoints[0].distance_to(linepoints[1])
	LineButtonInstance.rect_global_position = linepoints[0] - LineButtonInstance.rect_pivot_offset
	LineButtonInstance.rect_rotation = rad2deg(linepoints[1].angle_to_point(linepoints[0]))

func switch_line(linestring : String) -> void:
	show_lines()
	for child in get_children():
		child.visible = Lines[linestring] == child

func show_lines() -> void:
	LineButtonInstance.show()
	show()

func hide_lines() -> void:
	LineButtonInstance.hide()
	hide()
