extends Label

onready var Game = get_node("/root/Game")

func _process(_delta : float) -> void:
	if Game.HoveredStartOutpost != null:
		show()
		if Game.HoveredEndOutpost == null:
			rect_position = Game.CursorPosition - rect_pivot_offset - Vector2(0, 20)
		else:
			rect_position = Game.CursorPosition - rect_pivot_offset - Vector2(0, 80)
		# TODO: fix
		text = str(VariableFunctions.get_time_text(ceil(Game.HoveredStartOutpost.InitialPosition.distance_to(Game.CursorPosition) / GameVariables.SubmarineSpeed * GameVariables.TickLength / 10) * 10))
